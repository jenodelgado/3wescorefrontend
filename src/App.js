import React, { Component } from 'react';
import { Provider } from 'react-redux';

import Routes from './routes';
import store from "./services/handlers/store";

export default class App extends Component {
  constructor(props) {
        super(props)
        this.state = { loading: true }
    }

    render() {
        return (
            <Provider store={ store } >
                <Routes />
            </Provider>
        );
    }
}
