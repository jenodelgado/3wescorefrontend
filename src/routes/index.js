import React, { Component } from 'react'
import { BrowserRouter, Route } from 'react-router-dom'
import ContactUs from '../scenes/pages/ContactUs'
import ThemeScene from '../scenes/Theme'
import HomePage from '../scenes/pages/Home'
import MatchPage from '../scenes/pages/Match'
import FootballPage from '../scenes/pages/FootballPage'
import ForTesting from '../scenes/pages/ForTesting'
import BasketBallmatchpage from '../scenes/pages/BasketBallmatchpage'
import TermsOfService from '../scenes/pages/TermsOfService'
import H2H from '../scenes/pages/H2H'
import Oddds from '../scenes/pages/Oddds'
import Lineups from '../scenes/pages/Lineups'
import Standinggs from '../scenes/pages/Standinggs'
import FootballMatchPage from '../scenes/modules/FootballMatches'
import Dashboard from '../scenes/pages/Dashboard'
import {PrivateRoute} from '../../src/PrivateRoute'


class Router extends Component {
	render() {
		return (
			<BrowserRouter>
			
				<Route exact path="/" component={ HomePage } />
				<Route path="/match" component={ MatchPage } />
				<Route path="/footballPage" component={FootballPage} />
				<Route path="/basketBallmatchpage"	component={BasketBallmatchpage} />
				<Route path="/termsOfService" component={TermsOfService}/>
				<Route path="/contactUs" component={ContactUs}/>
				<Route path="/h2h" component={H2H} />
				<Route path="/oddds" component={Oddds} />
				<Route path="/lineups" component={Lineups}/>
				<Route path="/Standinggs" component={Standinggs} />
				<Route path ='/football' component={FootballPage} />
				<Route path ='/fortesting' component={ForTesting} />
			</BrowserRouter>
		)
	}
}

export default Router