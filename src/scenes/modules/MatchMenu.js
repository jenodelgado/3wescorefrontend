import React, { Component } from 'react'
import { Link, HashRouter, Switch } from 'react-router-dom'
import '../../assets/components/matchmenu.css'
import '../../assets/themeSelector.css'

class StartMatchTitle extends Component {

   

   render() {
      return (
         <>
		 
            		<div class="tab-bar justify-content no-border glassbg py-2 mobile" data-v-a4431e04="" data-v-69d97a86="">
						<div class="child" data-v-a4431e04="">
							<center>
							<Link to="lineups" class="links links--elara" data-v-a4431e04=""> Lineups </Link>
            				<Link to="oddds" class="links links--elara" data-v-a4431e04=""> Odds </Link>
            				<Link to="h2h" class="links links--elara" data-v-a4431e04=""> H2H </Link>
				            <Link to="standinggs" class="links links--elara" data-v-a4431e04=""> Standings </Link>
				            <Link to="/" class="links links--elara" data-v-a4431e04=""> Live </Link>
							</center>
				        </div>
					</div>

					{/* start mobile view */}
				
					<div className="container">
					<div className="tab-bar justify-content no-border glassbg py-2 mobileview" data-v-a4431e04="" data-v-69d97a86="">
						<div class="child" data-v-a4431e04="">
							<div className="row">
								<div className="col-3 " style={{paddingLeft:'15px'}}><Link to="lineups" class="links links--elara" data-v-a4431e04=""> <span className="xxsm">Lineups</span> </Link></div>
            			<div className="col-2 padding-l5  "><Link to="oddds" class="links links--elara" data-v-a4431e04="">  <span className="xxsm">Odds</span> </Link></div>
            			<div className="col-2 padding-l5 ">	<Link to="h2h" class="links links--elara" data-v-a4431e04="">  <span className="xxsm">H2H </span></Link> </div>
				         <div className="col-3 nopad  center">   <Link to="standinggs" class="links links--elara" data-v-a4431e04=""> <span className="xxsm"> Standings</span> </Link> </div>
				            <div className="col-1 padding-l5 "><Link to="/" class="links links--elara" data-v-a4431e04=""> <span className="xxsm">Chat</span> </Link></div>
							</div>
						</div>

					</div>
					</div>
         </>
      );
   }
}

export default StartMatchTitle