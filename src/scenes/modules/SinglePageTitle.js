import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import RenderIf from 'render-if'
import '../../assets/components/singlepage.css'
import '../../assets/themeSelector.css'

class SinglePageTitle extends Component {

   constructor(props) {
      super(props)
      this.state = { matches: [] }
   }

   componentWillMount() {
      this.setState({ matches: this.props.matches })
   }

   componentWillReceiveProps(nextProps) {
      this.setState({ matches: nextProps.matches })
   }

   /**
   * TO DO: Move this outside and make this common.
   */
   padSingleNumber(number) {
      return (number < 10 ? '0' : '') + number
   }

   diff_minutes(dt2, dt1) {
      let diff = (dt2.getTime() - dt1.getTime()) / 1000
            diff /= 60
            diff /= 60
      return Math.abs(Math.ceil(diff))
   }

   generateTableData() {
      const { language } = this.props
      return this.state.matches.map( match => {
         let now = new Date()
         let start = new Date(match.TimeStart)
         let stop = new Date(match.TimeStop)
         let matchTime = this.diff_minutes(now, start)
         
         return (
            <>
            <div className="mobile">
               <div className="row small pt-2">
                  <div className="col-sm-1">{ `${this.padSingleNumber(start.getHours())}:${this.padSingleNumber(start.getMinutes())}` }</div>
                  <div className="col-sm-1 center lightgold-text">{ (match.IsLive) ? `${ matchTime }'` : '-' }</div>
                  <div className="col-sm-3 right">{ match[`Home${language}`] }</div>
                  <div className="col-sm-3 lightgold-text x-medium strong">
                     <div className="row">
                        <div className="col-sm-4 right">{ match.HomeScore}</div>
                        <div className="col-sm-4 center"> - </div>
                        <div className="col-sm-4">{ match.AwayScore }</div>
                     </div>
                  </div>
                  <div className="col-sm-3">{ match[`Away${language}`] }</div>
                  <div className="col-sm-1 pb-1">
                     <div className="row">
                        <div className="col-sm-6">
                           { RenderIf(match.Type == 'FOOTBALL')(<Link to="/"><i className ="fa fa-futbol-o green-text medium"></i></Link>) }
                        </div>
                        <div className="col-sm-6">
                           {RenderIf(match.IsLive)(<Link to={{ pathname: "/match", state: { match } }}><i className ="far fa-play-circle red-text medium"></i></Link>)}
                           {RenderIf(!match.IsLive)(<i className ="fa fa-caret-square-o-right gray-text medium"></i>)}
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            
            
            {/* start for mobile view */}
            <div className="mobileview px-2">
               <div className="row no-gutters" >
                  <div className="col-2">
                     <div className="row">
                        <div className="col-12 nopad py-1 left wrapper small">
                           { `${this.padSingleNumber(start.getHours())}:${this.padSingleNumber(start.getMinutes())}` }
                        </div>
                        <div className="col-12 nopad py-1 lightgold-text left wrapper small">
                           { (match.IsLive) ? `${ matchTime }'` : '-' }
                        </div>
                     </div>
                  </div>
                  <div className="col-7">
                     <div className="row">
                        <div className="col-12 nopad py-1 left wrapper small"> { match[`Home${language}`]} </div>
                        <div className="col-12 nopad py-1 left wrapper small"> {  match[`Away${language}`]} </div>
                     </div>
                  </div>
                  <div className="col-1">
                     <div className="row">
                        <div className="col-12 nopad py-1 center wrapper lightgold-text x-medium strong"> { match.HomeScore} </div>
                        <div className="col-12 nopad py-1 center wrapper lightgold-text x-medium strong"> { match.AwayScore} </div>
                     </div>
                  </div>
                  <div className="col-1">
                     <div className="row height-100">
                        <div className="col-12 center my-auto">
                           {RenderIf(match.IsLive)(<Link to={{ pathname: "/match", state: { match } }}><i className ="far fa-play-circle red-text xx-large"></i></Link>)}
                           {RenderIf(!match.IsLive)(<i className ="fa fa-caret-square-o-right gray-text medium padding-l5"></i>)}
                        </div>
                     </div>
                  </div>
               </div>
            </div>

            {/* end for mobile view */}
         </>)
      } )
   }

   render() {
      return (
         <div className="px-2" style={{ width: '100%' }}>
            { this.generateTableData() }
         </div>
      )
   }
}

export default SinglePageTitle