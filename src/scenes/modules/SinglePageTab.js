import React, { Component } from 'react'
import '../../assets/components/singlepagetab.css'
import { Link } from 'react-router-dom'

class SinglePageTab extends Component{
    constructor(props){
        super(props)
        this.state = {
            teams: [
                { id: 1, teamName: 'Manchester City', coachName: 'Pepito', playerNumber:'12',playerName:'bebot', playerImage: 'sample1.png'}
                ]
        }
}
renderTableData(){
    return this.state.teams.map((Teams, index) =>{

        const {id, teamName, coachName, playerNumber, playerName, playerImage } = Teams
        return(
            <div className="tab-bar justify-content">
                <div className="content-box">
                    <div className="child">
                        <a href="/Match" className="tab mr-20 active" target>Lineups</a>
                        <a href="#!" className="tab mr-20 active" target>Odds</a> 
                        <Link to='/H2H'>H2H</Link>
                        <a href="#!" className="tab mr-20 active" target>Standings</a>
                        <a href="#!" className="tab mr-20 active" target>Live</a>
                    </div>
                </div>
            </div>

            )

        })
    
    }

render() {
    return (
       <div >
            <div className="container">
          <table id='Matches'style={{width:'inherit'}}>
             <tbody>
                {this.renderTableData()}
                               
             </tbody>
          </table>
          </div>
       </div>
    )
 }
}
export default SinglePageTab