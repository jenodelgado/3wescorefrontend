import React, { Component } from 'react'
import '../../assets/components/odss.css'
import '../../assets/themeSelector.css'

 class Odds extends Component {
    render() {
        return (
            <>
          <div className="mobile">
            <div className="container">
                <div className="bax">
                    <div className="ods">
                        <span className="odds-update">Odds</span>
                    </div>
                    <div className="row">
                        <div className="col-md-4 ">
                            <div className="table-odds">
                                <span className="heads">1X2</span>
                                <div className="odds-row">
                                    <div className="ocolumns ocolumn1"><i className="fa fa-clock" /></div>
                                    <div className="ocolumns ocolumn2"> <i className="fa fa-futbol" /></div>
                                    <div className="ocolumns ocolumn3">Win</div>
                                    <div className="ocolumns ocolumn4">Draw</div>
                                    <div className="ocolumns ocolumn5">Lose</div>
                                    <div className="ocolumns ocolumn6">Time</div>
                                </div>
                                <div className="odds-row">
                                    <div className="ocolumns"></div>
                                    <div className="ocolumn">0-0</div>
                                    <div className="ocolumne">1.22
                                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAAMBAMAAACkW0HUAAAAFVBMVEUAAADoTVnmTFrnTFrnTFvoS1rnTFtScnYmAAAABnRSTlMAFeJ/diJJeCYpAAAAH0lEQVQI12PAAVgSMSnmtDRVEG2WFADmqkKUCqDrBQCDPANEPKAFggAAAABJRU5ErkJggg==" />
                                    </div>
                                    <div className="ocolumnes">6.0
                                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAAMBAMAAACkW0HUAAAAKlBMVEUAAABeuyJRrjBRsi5VsS1Sry9TsC9SrzBTsTBSry9SsC5RszFVsipSry9j6G+BAAAADXRSTlMAB0VCGPa5drCZVDkerbt6cwAAACdJREFUCNdjwAGYGsGU6jUBECf2biKQ4ra8ZQISZL/CgETxGKLrBQAC+wZxXW/6ogAAAABJRU5ErkJggg=="/>
                                    </div>
                                    <div className="ocolumn">8.0</div>
                                    <div className="ocolumn">03/10 08:39</div>
                                </div> 
                            </div>
                        </div>
                        <div className="col-md-4 ">
                            <div className="table-odds">
                                <div className="heads">
                                    <div className="between-tunga">
                                        <span>Ha</span>
                                    </div>
                                </div>
                                <div className="odds-row">
                                    <div className="ocolumns ocolumn1"><i className="fa fa-clock" /></div>
                                    <div className="ocolumns ocolumn2"> <i className="fa fa-futbol" /></div>
                                    <div className="ocolumns ocolumn3">Home</div>
                                    <div className="ocolumns ocolumn4">Handicap</div>
                                    <div className="ocolumns ocolumn5">Away</div>
                                    <div className="ocolumns ocolumn6">Time</div>
                                </div>
                                <div className="odds-row">
                                    <div className="ocolumns"></div>
                                    <div className="ocolumn">0-0</div>
                                    <div className="ocolumn">1.22</div>
                                    <div className="ocolumn">6.0</div>
                                    <div className="ocolumn">8.0</div>
                                    <div className="ocolumn ">03/10 08:39</div>
                                </div> 
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="table-odds">
                                <span className="heads">O/U</span>
                                <div className="odds-row">
                                    <div className="ocolumns ocolumn1"><i className="fa fa-clock" /></div>
                                    <div className="ocolumns ocolumn2"> <i className="fa fa-futbol" /></div>
                                    <div className="ocolumns ocolumn3">Over</div>
                                    <div className="ocolumns ocolumn4">Goals</div>
                                    <div className="ocolumns ocolumn5">Under</div>
                                    <div className="ocolumns ocolumn6">Time</div>
                                </div>
                                <div className="odds-row">
                                    <div className="ocolumns"></div>
                                    <div className="ocolumn">0-0</div>
                                    <div className="ocolumnes">1.22
                                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAAMBAMAAACkW0HUAAAAKlBMVEUAAABeuyJRrjBRsi5VsS1Sry9TsC9SrzBTsTBSry9SsC5RszFVsipSry9j6G+BAAAADXRSTlMAB0VCGPa5drCZVDkerbt6cwAAACdJREFUCNdjwAGYGsGU6jUBECf2biKQ4ra8ZQISZL/CgETxGKLrBQAC+wZxXW/6ogAAAABJRU5ErkJggg==" />
                                    </div>
                                    <div className="ocolumn">6.0</div>
                                    <div className="ocolumn">8.0</div>
                                    <div className="ocolumn ">03/10 08:39</div>
                                </div> 
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            </div>
            
            
            <div className="mobileview">
                <div className="container nopad">
                    <div className="match nopad">
                    <div className="match nopad">
                        <span className="match-update nopad xxsm">Odds</span>&nbsp;
                    </div>
                         <div className="row">
                            <div className="col-md-12 ">
                                <div className="table-odds">
                                <span className="nopad xxsm">1X2</span>
                                    <div className="odds-row">
                                        <div className="ocolumns ocolumn1 nopad xxsm"><i className="fa fa-clock" /></div>
                                        <div className="ocolumns ocolumn1 nopad xxsm"><i className="fa fa-futbol" /></div>
                                        <div className="ocolumns ocolumn3 nopad xxsm">Win</div>
                                        <div className="ocolumns ocolumn4 nopad xxsm">Draw</div>
                                        <div className="ocolumns ocolumn5 nopad xxsm">Lose</div>
                                        <div className="ocolumns ocolumn6 nopad xxsm">Time</div>
                                    </div>
                                    <div className="odds-row">
                                        <div className="ocolumns nopad xxsm"></div>
                                        <div className="ocolumn nopad xxsm">0-0</div>
                                        <div className="ocolumn nopad xxsm">1.22</div>
                                        <div className="ocolumn nopad xxsm">6.0</div>
                                        <div className="ocolumn nopad xxsm">8.0</div>
                                    <div className="ocolumns nopad xxsm wrapper">03/10 08:39</div>
                                </div> 
                                </div>
                            </div>
                         </div>
                         <div className="row">
                            <div className="col-md-12 ">
                                <div className="table-odds">
                                <span className="nopad xxsm">Hand</span>
                                    <div className="odds-row">
                                        <div className="ocolumns ocolumn1 nopad xxsm"><i className="fa fa-clock" /></div>
                                        <div className="ocolumns ocolumn1 nopad xxsm"><i className="fa fa-futbol" /></div>
                                        <div className="ocolumns ocolumn3 nopad xxsm">Win</div>
                                        <div className="ocolumns ocolumn4 nopad xxsm">Draw</div>
                                        <div className="ocolumns ocolumn5 nopad xxsm">Lose</div>
                                        <div className="ocolumns ocolumn6 nopad xxsm">Time</div>
                                    </div>
                                    <div className="odds-row">
                                        <div className="ocolumns nopad xxsm"></div>
                                        <div className="ocolumn nopad xxsm">0-0</div>
                                        <div className="ocolumn nopad xxsm">1.22</div>
                                        <div className="ocolumn nopad xxsm">6.0</div>
                                        <div className="ocolumn nopad xxsm">8.0</div>
                                    <div className="ocolumns nopad xxsm wrapper">03/10 08:39</div>
                                </div> 
                                </div>
                            </div>
                         </div>     
                         <div className="row">
                            <div className="col-md-12 ">
                                <div className="table-odds">
                                <span className="nopad xxsm">1X2</span>
                                    <div className="odds-row">
                                        <div className="ocolumns ocolumn1 nopad xxsm"><i className="fa fa-clock" /></div>
                                        <div className="ocolumns ocolumn1 nopad xxsm"><i className="fa fa-futbol" /></div>
                                        <div className="ocolumns ocolumn3 nopad xxsm">Win</div>
                                        <div className="ocolumns ocolumn4 nopad xxsm">Draw</div>
                                        <div className="ocolumns ocolumn5 nopad xxsm">Lose</div>
                                        <div className="ocolumns ocolumn6 nopad xxsm">Time</div>
                                    </div>
                                    <div className="odds-row">
                                        <div className="ocolumns nopad xxsm"></div>
                                        <div className="ocolumn nopad xxsm">0-0</div>
                                        <div className="ocolumn nopad xxsm">1.22</div>
                                        <div className="ocolumn nopad xxsm">6.0</div>
                                        <div className="ocolumn nopad xxsm">8.0</div>
                                    <div className="ocolumns nopad xxsm wrapper">03/10 08:39</div>
                                </div> 
                                </div>
                            </div>
                         </div>          
                    </div>
                    
                </div>
            </div>
            
            </>
        )
    }
}
export default Odds
