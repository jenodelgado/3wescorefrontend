import React, { Component } from 'react'
import Message from './Message';

class MessageList extends Component {
    render() {
        return (
            <div>
                { 
                    this.props.messages.map( message => 
                        <Message timestamp={message.timestamp}
                        owner={message.owner}
                        key={message.id}/>
                      )
                }
            </div>
        );
    }
}

export default MessageList