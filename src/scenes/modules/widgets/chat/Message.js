import React, { Component } from 'react'


class Message extends Component {
    render() {
        let now = new Date( this.props.timestamp );
        let hhmmss = now.getHours() + ':' + now.getMinutes() + ':' + now.getSeconds(); 
        return (
            <div className="message">
                <span className="message-time">{hhmmss}</span>&nbsp; 
                <strong className="message-owner">{this.props.owner}</strong>&nbsp;
                <span className="message-text">{this.props.text}</span>
            </div>
        );
    }
}

export default Message