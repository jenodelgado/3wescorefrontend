import React, { Component } from 'react'
import swal from 'sweetalert';



class PostMessageForm extends Component {
    state={
        message: "",
        owner: true,

    };
	constructor( props, context ) {
		super( props, context );

		this.handleSubmit = this.handleSubmit.bind( this );
	}
    handleSubmit( event ) {
        event.preventDefault();
        if(this.state.owner){
            this.props.toggleLoginModal(true)
           
               if(this.state.message===""){
                    swal({
                    title: "Error!",
                    text: "Cannot Send empty messages",
                    icon: "error",
                    });
                }
           
            } else{

                this.props.appendChatMessage(this.state.message );
                this.setState({message:""});
                this.setState({owner:""})    
           
        }
    }
    onChange = event => this.setState({ message: event.target.value });	
    onChange2 = event => this.setState({ owner: event.target.value });	
    render() {
        return (
            <>
            <form onSubmit={this.handleSubmit}>
                <div className="row">
                    <div className="col-9 padding-l5 padding-r5">  

                        <input type="text" 
                            name='message'
                            className="form-control"
                            placeholder="Message"
                            value={this.state.message}
                            onChange={this.onChange} />

                        <input type="hidden" 
                            name='owner'
                            className="form-control"
                            placeholder="owner"
                            value={this.state.owner}
                            onChange={this.onChange} />
                    </div>
                    <div className="col-3 nopad">
                      <input type="submit" className="btn btn-dark width-100" value="Send" />
                    </div>
                </div>
            </form>
            </>
       );
    }
}

export default PostMessageForm