import React, { Component } from 'react'
import Modal from 'react-modal';
import MessageList from './MessageList';
import PostMessageForm from './PostMessageForm';
import LoginForm from '../../LoginForm';
import '../../../../assets/components/chat.css';
import '../../../../assets/modal.css';

class ChatBox extends Component {
    constructor( props, context ) {
        super( props, context );
        this.state = {
            messages: [],
            isShowLogin: false
        };
        
        this.appendChatMessage = this.appendChatMessage.bind( this );
        this.toggleLoginModal = this.toggleLoginModal.bind(this);
    }
    
    appendChatMessage( owner, text ) {
        let newMessage = {
            id: this.state.messages.length + 1,
            timestamp: new Date().getTime(),
            owner: owner,
            text: text
        }; 
        this.setState({ messages: [ ...this.state.messages, newMessage ] });
    }    
    clearMessages() {
        this.setState( { messages: [] } );
    }
    toggleLoginModal(boolean) {
        this.setState({ isShowLogin: boolean })
    }
    render() {
        const { isShowLogin } = this.state
        return ( <>
            <div className="messgae-box height-100">
                <div className="title">
                    <span className="text">Chatroom</span>
                </div>
                <div className="msg-content">   
                    <p> <MessageList messages={this.state.messages}/></p>
                    <div style={{ float:"left", clear: "both" }} ref={(el) => { this.messagesEnd = el; }}> </div>   
                </div>
                <div className="send-box">
                    <PostMessageForm appendChatMessage={this.appendChatMessage} toggleLoginModal={this.toggleLoginModal} />
                </div>
                
                <Modal className='centered1' isOpen={isShowLogin}>
                    <button to='/login' onClick={() => this.toggleLoginModal(false)} className="btnx">x</button>
                    <LoginForm></LoginForm>
                </Modal>
            </div>
        </> )
        
    }
        scrollToBottom = () => {
        this.messagesEnd.scrollIntoView(false);
      }
      
      componentDidMount() {
        this.scrollToBottom();
      }
      
      componentDidUpdate() {
        this.scrollToBottom();
      }

}

export default ChatBox