import React, { Component } from 'react'
import '../../assets/components/standings.css'

 class Standings extends Component {
    render() {
        return (
            <div className="container">
              <div className="standingbox">
                  <div className="row">
                    <div className="standingtitle">STANDINGS</div>
                      <div className="standingbox">
                        <div className="row">
                            <div className="perteambox">
                                <div className="row">
                            <div className="col-sm-1"></div>
                            <div className="col-sm-4"></div>
                            <div className="col-sm-1"><p className="detailstext">P</p></div>
                            <div className="col-sm-1"><p className="detailstext">W</p></div>
                            <div className="col-sm-1"><p className="detailstext">D</p></div>
                            <div className="col-sm-1"><p className="detailstext">L</p></div>
                            <div className="col-sm-1"><p className="detailstext">Pts</p></div>
                            </div>
                            </div>
                                <div className="perteambox">
                                    <div className="row">
                                < div className="col-1"><i class="fab fa-battle-net fa-s"></i> </ div>
                                < div className="col-4"><p className="detailstext">Home Team</p> </ div>
                                < div className="col-1"><p className="detailstext" >21</p> </ div>
                                < div className="col-1"><p className="detailstext">32</p> </ div>
                                < div className="col-1"><p className="detailstext">30</p> </ div>
                                < div className="col-1"> <p className="detailstext">21</p></ div>
                                < div className="col-1"> <p className="detailstext">104</p></ div>
                                    </div>
                                </div>
                                
                                <div className="perteambox">
                                    <div className="row">
                                < div className="col-1"><i class="fas fa-biohazard fa-s"></i> </ div>
                                < div className="col-4"><p className="detailstext">Away Team</p> </ div>
                                < div className="col-1"><p className="detailstext" >21</p> </ div>
                                < div className="col-1"><p className="detailstext">32</p> </ div>
                                < div className="col-1"><p className="detailstext">30</p> </ div>
                                < div className="col-1"> <p className="detailstext">21</p></ div>
                                < div className="col-1"> <p className="detailstext">104</p></ div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                      </div>
                    </div>
                </div>
        
        )
    }
}
export default Standings
