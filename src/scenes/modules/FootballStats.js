import React, { Component } from 'react'
import { Link } from 'react-router-dom'

import ProgressBar from 'react-bootstrap/ProgressBar'
import '../../assets/components/bar.css'

class Stat extends Component {

   

   render() {
      return (
         <>
         
         <div className="container">
           
               <div className="boxstats">
           <div className="row">
              <div className="matchupss">
                 <div className="row">
                   <div className="col-sm-3 right-text">
                      <label> Home Team</label>
                   </div>
                   <div className="col-sm-1 left-text">
                       <i class="fab fa-battle-net fa-xl"></i>
                     </div>
                   <div className="col-sm-4 center-text">
                      <label>Stats</label>
                   </div>
                    <div className="col-sm-1 right-text" >
                      <i class="fas fa-biohazard fa-xl"></i>
                   </div>
                   <div className="col-sm-3 left-text" >
                      <label>Away Team</label>
                   </div>
                  </div>
            </div>
            
            <div className="row">       
                   <div className="col-sm-4 right-text">
                      <label>58</label>
                   </div>                   
                   <div className="col-sm-4 center-text">
                      <label>Possession</label>
                   </div>
                   <div className="col-sm-4 left-text" >
                      <label>42</label>
                   </div>
            </div>
             <div className="row">
                   <div className="col-sm-6">
                     <ProgressBar variant="danger" className="binary-progress" style={{height:'7px'}} now={58} />
                   </div>
                   <div className="col-sm-6">
                     <ProgressBar variant="warning" style={{height:'7px'}} now={42} />
                   </div>
         </div>        
            <div className="row">      
                   <div className="col-sm-4 right-text">
                      <label>6</label>
                   </div>                  
                   <div className="col-sm-4 center-text">
                      <label>Shots</label>
                   </div>
                   <div className="col-sm-4 left-text" >
                      <label>11</label>
                   </div>
            </div>
             <div className="row">
                   <div className="col-sm-6" >
                     <ProgressBar variant="danger" className="binary-progress" style={{height:'7px'}} now={35} />
                   </div>
                   <div className="col-sm-6">
                     <ProgressBar variant="warning" style={{height:'7px'}} now={65} />
                   </div>
         </div>      
            <div className="row">
                   <div className="col-sm-4 right-text">
                      <label>2</label>
                   </div>               
                   <div className="col-sm-4 center-text">
                      <label>Shots on Target</label>
                   </div>
                   <div className="col-sm-4 left-text" >
                      <label>0</label>
                   </div>
            </div>
             <div className="row">
                   <div className="col-sm-6" >
                     <ProgressBar variant="danger" className="binary-progress" style={{height:'7px'}} now={100} />
                   </div>
                   <div className="col-sm-6">
                     <ProgressBar variant="warning" style={{height:'7px'}} now={0} />
                   </div>
         </div>        
            <div className="row">           
                   <div className="col-sm-4 right-text">
                      <label>58</label>
                   </div>
                 
                   <div className="col-sm-4 center-text">
                      <label>Attacks</label>
                   </div>
                   <div className="col-sm-4 left-text" >
                      <label>42</label>
                   </div>
            </div>
             <div className="row">
                   <div className="col-sm-6" >
                     <ProgressBar variant="danger" className="binary-progress" style={{height:'7px'}} now={58} />
                   </div>
                   <div className="col-sm-6">
                     <ProgressBar variant="warning" style={{height:'7px'}} now={42} />
                   </div>
         </div>
         
         
            <div className="row">
               
                   <div className="col-sm-4 right-text">
                      <label>58</label>
                   </div>
                   
                   <div className="col-sm-4 center-text">
                      <label>Dangerous Attacks</label>
                   </div>
                   <div className="col-sm-4 left-text" >
                      <label>42</label>
                   </div>
            </div>
             <div className="row">
                   <div className="col-sm-6" >
                     <ProgressBar variant="danger" className="binary-progress" style={{height:'7px'}} now={58} />
                   </div>
                   <div className="col-sm-6">
                     <ProgressBar variant="warning" style={{height:'7px'}} now={42} />
                   </div>
         </div>
         
         
            <div className="row">
               
                   <div className="col-sm-4 right-text">
                      <label>58</label>
                   </div>
                   
                   <div className="col-sm-4 center-text">
                      <label>Free Kicks</label>
                   </div>
                   <div className="col-sm-4 left-text" >
                      <label>42</label>
                   </div>
            </div>
             <div className="row">
                   <div className="col-sm-6" >
                     <ProgressBar variant="danger" className="binary-progress" style={{height:'7px'}} now={58} />
                   </div>
                   <div className="col-sm-6">
                     <ProgressBar variant="warning" style={{height:'7px'}} now={42} />
                   </div>
         </div>
         
            <div className="row">
               
                   <div className="col-sm-4 right-text">
                      <label>58</label>
                   </div>
                   
                   <div className="col-sm-4 center-text">
                      <label>Corners</label>
                   </div>
                   <div className="col-sm-4 left-text" >
                      <label>42</label>
                   </div>
            </div>
             <div className="row">
                   <div className="col-sm-6" >
                     <ProgressBar variant="danger" className="binary-progress" style={{height:'7px'}} now={58} />
                   </div>
                   <div className="col-sm-6">
                     <ProgressBar variant="warning" style={{height:'7px'}} now={42} />
                   </div>
         </div>
         
            <div className="row">
               
                   <div className="col-sm-4 right-text">
                      <label>58</label>
                   </div>
                   
                   <div className="col-sm-4 center-text">
                      <label>Yellow cards</label>
                   </div>
                   <div className="col-sm-4 left-text" >
                      <label>42</label>
                   </div>
            </div>
             <div className="row">
                   <div className="col-sm-6" >
                     <ProgressBar variant="danger" className="binary-progress" style={{height:'7px'}} now={58} />
                   </div>
                   <div className="col-sm-6">
                     <ProgressBar variant="warning" style={{height:'7px'}} now={42} />
                   </div>
         </div>
         
            <div className="row">
               
                   <div className="col-sm-4 right-text">
                      <label>58</label>
                   </div>
                   
                   <div className="col-sm-4 center-text">
                      <label>Red Cards</label>
                   </div>
                   <div className="col-sm-4 left-text" >
                      <label>42</label>
                   </div>
            </div>
             <div className="row">
                   <div className="col-sm-6" >
                     <ProgressBar variant="danger" className="binary-progress" style={{height:'7px'}} now={58} />
                   </div>
                   <div className="col-sm-6">
                     <ProgressBar variant="warning" style={{height:'7px'}} now={42} />
                   </div>
         </div>
         </div>
         </div>
      </div>
         </>
      );
   }
}

export default Stat