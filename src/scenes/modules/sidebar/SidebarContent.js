import React, { Component } from 'react'
import '../../../assets/sidebars.css'
import Accordion from '../accordion/Accordion'

class SidebarContent extends Component {
	
	render() {
		const {language} = this.state
		return (
            <div contentClass="glassbg"
				containerClass={`col-md-12 px-3 py-3 gold-text sidebar ${(this.props.with_margin) ? 'mt-1' : 'd'}`} 
				label={this.props.title}>
              <hr />
              <div> {this.props.content} </div>
            </div>
		)
	}
}

export default SidebarContent