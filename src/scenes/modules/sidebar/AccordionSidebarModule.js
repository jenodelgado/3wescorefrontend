import React, { Component } from 'react'
import Accordion from '../accordion/Accordion'
import { Link } from 'react-router-dom'
import SidebarModule from './SidebarModule'

class AccordionSidebarModule extends Component {
	constructor(props) {
		super(props)

		this.state = {
			leagues: this.props.leagues,
			language: ''
		}
		this.setLanguage = this.setLanguage.bind(this)
	}

	componentWillReceiveProps(nextProps) {
		this.setState({ leagues: nextProps.leagues })
	}
	setLanguage(lang){
		this.setState({language: lang})
	}

	render() {
		const { leagues } = this.state

		let sidebar = [
			{ title: 'Live Matches', content: leagues.live, with_margin: false},
			{ title: 'Leagues', content: leagues.all, with_margin: true },
		]

		if (this.props.sport == 'HORSERACING') 
			return this.generateChannels()

		return (<>
			<Accordion allowMultipleOpen setLanguage={this.setLanguage}>
				{ sidebar.map(d => ( 
					<div contentClass="glassbg py-4 "
						containerTitleClass={`col-md-12 px-3 py-3 gold-text sidebar ${(d.with_margin) ? 'mt-1' : ''}`} label={d.title}>
						
						{ this.generateLeagues(d.content) }
		            </div>
				)) }
			</Accordion>
		</> )
	}

	generateChannels() {
		let channels = [
			{ League: 'Horse Racing', Name: 'UK ATR', Channel: 1014 },
			{ League: 'Horse Racing', Name: 'UK RACING', Channel: 1015 },
			{ League: 'Horse Racing', Name: 'MACAU', Channel: 1016 },
			{ League: 'Horse Racing', Name: 'AUSB SKY2', Channel: 1018 },
			{ League: 'Horse Racing', Name: 'AUS SKY1', Channel: 1019 },
			{ League: 'Horse Racing', Name: 'AUS SKY2', Channel: 1020 },
			{ League: 'Horse Racing', Name: 'SG 88 CN', Channel: 1021 },
			{ League: 'Horse Racing', Name: 'SG 88 EN/HK EN', Channel: 1022 },
			{ League: 'Horse Racing', Name: 'SG 89', Channel: 1023 },
			{ League: 'Horse Racing', Name: 'SG 89', Channel: 1024 },
			{ League: 'Horse Racing', Name: 'AUSB SKY1', Channel: 1025 }
		]

		return (<Accordion allowMultipleOpen setLanguage={this.setLanguage}>
			<div contentClass="glassbg py-4 "
				containerTitleClass={`col-md-12 px-3 py-3 gold-text sidebar `} label="Horse Racing Channels">
				{ channels.map(c => (<div className="px-2 mb-1 white-text" style={{ 'fontSize': '12pt' }}>
					<Link to={{ pathname: "/match", state: { match: c } }}><i className ="far fa-play-circle red-text xx-large"></i></Link> &nbsp;
					<span className="py-4">{c.Name}</span>
				</div>)) }
            </div>
		</Accordion>)
	}

	generateLeagues(contents) {
		const {language} = this.state
		return contents.map(c => {
			let name = c[`name${language}`].split(/-(.+)/)
			return (<div className="px-2 mb-1" style={{ 'font-size': '10pt' }}>
      			<a data-v-52339fd3="" target="_blank" class="comps-item white-text" style={{ 'text-decoration': 'none', 'cursor': 'pointer' }} language={language}>
	      			<div data-v-52339fd3="" class="ml-xs w-o-h py-2"><span style={{ color: 'yellow' }}>{`${(name.length > 1) ? `${name[0]} : ` : ''}`}</span>{ (name.length > 1) ? name[1] : name[0] } </div>
      			</a>
      		</div>)
		})
	}
}

export default AccordionSidebarModule