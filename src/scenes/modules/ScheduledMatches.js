import React, { Component } from 'react'
import '../../assets/components/scheduledmatches.css'

 class ScheduledMatches extends Component {
    render() {
        return (
            <div className="container">
                <div className="boxs">
                    <div className="match">
                        <span className="match-update">Scheduled matches</span>
                    </div>
                    <div className="row">
                        <div className='col-md-6 asd'>
                            <div className="match-selecta">
                            <img src="https://img0.aiscore.com/football/team/72168634954ac54e101096c19c32aa1b.png!w30" />Liverpool
                            </div>
                            <div class="div-table">
                                <div class="trow">
                                    <div class="dcolumns dcolumn1">Leagues</div>
                                    <div class="dcolumns dcolumn2">Date</div>
                                    <div class="dcolumns dcolumn3">Home</div>
                                    <div class="dcolumns dcolumn4">Away</div>
                                    <div class="dcolumns dcolumn5">Days Until</div>
                                </div>
                                <div class="trow">
                                    <div class="dcolumne">English Premier League</div>
                                    <div class="dcolumn">2021/03/05</div>
                                    <div class="dcolumn">Liverpool</div>
                                    <div class="dcolumnes">0-1</div>
                                    <div class="dcolumn"></div>  
                                </div>
                                <div class="trow">
                                    <div class="dcolumne">English Premier League</div>
                                    <div class="dcolumn">2021/03/05</div>
                                    <div class="dcolumn">Chelsea</div>
                                    <div class="dcolumnee">0-0</div>
                                    <div class="dcolumn"></div>
                                </div>
                                <div class="trow">
                                    <div class="dcolumne">UEFA Champions League</div>
                                    <div class="dcolumn">2021/02/24</div>
                                    <div class="dcolumn">Atletico Madrid</div>
                                    <div class="dcolumnes">0-1</div>
                                    <div class="dcolumn"></div>  
                                </div>
                                <div class="trow">
                                    <div class="dcolumne">English Premier League</div>
                                    <div class="dcolumn">2021/02/20</div>
                                    <div class="dcolumn">Southampton</div>
                                    <div class="dcolumnee">1-1</div>
                                    <div class="dcolumn"></div>
                                </div>
                                <div class="trow">
                                    <div class="dcolumne">English Premier League</div>
                                    <div class="dcolumn">2021/02/16</div>
                                    <div class="dcolumn">Chelsea</div>
                                    <div class="dcolumnes">2-0</div>
                                    <div class="dcolumn"></div>
                                </div> 
                            </div>
                        </div>
                        <div className='col-md-6 asd-1'>
                        <div className="match-selecta">
                            <img src="https://img0.aiscore.com/football/team/798ccacd5ae2a04dc2a7ade7f2e6cfe4.png!w30" />RB Leipzig
                            </div>
                            <div class="div-table">
                                <div class="trow" style={{border: '1px solid red'}}>
                                    <div class="dcolumns dcolumn1">Leagues</div>
                                    <div class="dcolumns dcolumn2">Date</div>
                                    <div class="dcolumns dcolumn3">Home</div>
                                    <div class="dcolumns dcolumn4">Away</div>
                                    <div class="dcolumns dcolumn5">Days Until</div>
                                </div>
                                <div class="trow">
                                    <div class="dcolumne">English Premier League</div>
                                    <div class="dcolumn">2021/03/05</div>
                                    <div class="dcolumn">Liverpool</div>
                                    <div class="dcolumnes">0-1</div>
                                    <div class="dcolumn"></div>  
                                </div>
                                <div class="trow">
                                    <div class="dcolumne">English Premier League</div>
                                    <div class="dcolumn">2021/03/05</div>
                                    <div class="dcolumn">Chelsea</div>
                                    <div class="dcolumnee">0-0</div>
                                    <div class="dcolumn"></div>
                                </div>
                                <div class="trow">
                                    <div class="dcolumne">UEFA Champions League</div>
                                    <div class="dcolumn">2021/02/24</div>
                                    <div class="dcolumn">Atletico Madrid</div>
                                    <div class="dcolumnes">0-1</div>
                                    <div class="dcolumn"></div>  
                                </div>
                                <div class="trow">
                                    <div class="dcolumne">English Premier League</div>
                                    <div class="dcolumn">2021/02/20</div>
                                    <div class="dcolumn">Southampton</div>
                                    <div class="dcolumnee">1-1</div>
                                    <div class="dcolumn"></div>
                                </div>
                                <div class="trow">
                                    <div class="dcolumne">English Premier League</div>
                                    <div class="dcolumn">2021/02/16</div>
                                    <div class="dcolumn">Chelsea</div>
                                    <div class="dcolumnes">2-0</div>
                                    <div class="dcolumn"></div>
                                </div> 
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        )
    }
}
export default ScheduledMatches
