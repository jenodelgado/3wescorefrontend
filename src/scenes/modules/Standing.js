import React, { Component } from 'react'
import '../../assets/components/standing.css'
import { Tabs, TabList, Tab, TabPanel } from 'react-tabs'

 class Standing extends Component {

    	constructor(props) {
		super(props)

		this.state = {
			activeTab: 0
		}
	}

	setActiveTab(index) {
		this.setState({ activeTab: index })
	}
    render() {
        return (
            <>
            <div className="mobile">
            <div className="container">
                <div className="box">
                    <div className="match">
                        <span className="stand-match">Standings</span>&nbsp;
                    </div>
                    <div className="match-select">Last</div>
                    <div class="div-table">
                        <div class="trow">
                            <div class="tcolumns tcolumn1">#
                                <span>Team</span>
                            </div>
                            <div class="tcolumns tcolumn2">P W D L</div>
                            <div class="tcolumns tcolumn3">Goals</div>
                            <div class="tcolumns tcolumn4">Last 5</div>
                            <div class="tcolumns tcolumn5">Away</div>
                        </div>
                        <div class="trow">
                            <div class="tcolumne">English Premier League</div>
                            <div class="tcolumn">2021/03/05</div>
                            <div class="tcolumn">0-1</div>
                            <div class="tcolumnes">sdsd</div>
                            <div class="tcolumn">Chelsea</div>
                        </div>
                        <div class="trow">
                            <div class="tcolumne">English Premier League</div>
                            <div class="tcolumn">2021/03/05</div>
                            <div class="tcolumn">0-1</div>
                            <div class="tcolumnee">0-0</div>
                            <div class="tcolumn">Manchester United</div>
                            
                        </div>
                        <div class="trow">
                            <div class="tcolumne">UEFA Champions League</div>
                            <div class="tcolumn">2021/02/24</div>
                            <div class="tcolumn">0-1</div>
                            <div class="tcolumnes">0-1</div>
                            <div class="tcolumn">Chelsea</div>
                            
                        </div>
                        <div class="trow">
                            <div class="tcolumne">English Premier League</div>
                            <div class="tcolumn">2021/02/20</div>
                            <div class="tcolumn">1-1</div>
                            <div class="tcolumnee">1-1</div>
                            <div class="tcolumn">Chelsea</div>
                            
                        </div>
                        <div class="trow">
                            <div class="tcolumne">English Premier League</div>
                            <div class="tcolumn">2021/02/16</div>
                            <div class="tcolumn">1-1</div>
                            <div class="tcolumnes">2-0</div>
                            <div class="tcolumn">Newcastle United</div>
                            
                        </div> 
                    </div>
                    <div className="foot">
                            Last 5, Chelsea Win
                        <span className="color-green">3</span>
                            , Draw
                        <span className="color-yellow">2</span>
                            ,Lose
                        <span className="color-red">0</span>
                        , Score Win Prob : 60.00% 
                    </div>
                </div>
            </div>
            </div>

                {/* start of mobile view */}

                <div className="mobileview">
                    <div className="container nopad">
                     <div className="row nopad">
                        <div className="tab-bar justify-content no-border glassbg py-2 nopad">
                            	<Tabs selectedIndex={this.state.activeTab} onSelect={index => this.setActiveTab(index)}>
               <div className="nopad"> <TabList>
	                <Tab>All</Tab>
	                <Tab> home</Tab>
	                <Tab>Away</Tab>
	             </TabList> </div>
                        <div className="row">
                            <div className="col-12 xxsm text-white padding-l20">Australia A-League</div>
                        </div>
                        <div className="row py-2">
                            <div className="col-1 xxsm text-white padding-l20">#</div>
                            <div className="col-2 xxsm text-white">Team</div>
                              <div className="col-1 xxsm text-white ">P</div>
                                <div className="col-1 xxsm text-white ">W</div>
                                  <div className="col-1 xxsm text-white ">D</div>
                                    <div className="col-1 xxsm text-white ">L</div>
                                    <div className="col-2 xxsm text-white ">Goals</div>
                                    <div className="col-1 xxsm text-white ">Pts</div>
                                     </div>
                                      <div className="row ">
                                        <div className="col-1 xxsm text-white padding-l20">1</div>
                            <div className="col-2 xxsm text-white wrapper">Central Coast Mariners</div>
                              <div className="col-1 xxsm text-white ">12</div>
                                <div className="col-1 xxsm text-white ">8</div>
                                  <div className="col-1 xxsm text-white ">2</div>
                                    <div className="col-1 xxsm text-white ">1</div>
                                    <div className="col-2 xxsm text-white ">22:13</div>
                                    <div className="col-1 xxsm text-white ">25</div>
                        </div>
                           <div className="row ">
                                        <div className="col-1 xxsm text-white padding-l20">1</div>
                            <div className="col-2 xxsm text-white wrapper">westerb Sydney</div>
                              <div className="col-1 xxsm text-white ">12</div>
                                <div className="col-1 xxsm text-white ">8</div>
                                  <div className="col-1 xxsm text-white ">2</div>
                                    <div className="col-1 xxsm text-white ">1</div>
                                    <div className="col-2 xxsm text-white ">22:13</div>
                                    <div className="col-1 xxsm text-white ">25</div>
                        </div>
                           <div className="row ">
                                        <div className="col-1 xxsm text-white padding-l20">1</div>
                            <div className="col-2 xxsm text-white wrapper">Adeliade United </div>
                              <div className="col-1 xxsm text-white ">12</div>
                                <div className="col-1 xxsm text-white ">8</div>
                                  <div className="col-1 xxsm text-white ">2</div>
                                    <div className="col-1 xxsm text-white ">1</div>
                                    <div className="col-2 xxsm text-white ">22:13</div>
                                    <div className="col-1 xxsm text-white ">25</div>
                        </div>
                           <div className="row ">
                                        <div className="col-1 xxsm text-white padding-l20">1</div>
                            <div className="col-2 xxsm text-white wrapper">Melbourne City </div>
                              <div className="col-1 xxsm text-white ">12</div>
                                <div className="col-1 xxsm text-white ">8</div>
                                  <div className="col-1 xxsm text-white ">2</div>
                                    <div className="col-1 xxsm text-white ">1</div>
                                    <div className="col-2 xxsm text-white ">22:13</div>
                                    <div className="col-1 xxsm text-white ">25</div>
                        </div>
                </Tabs>
                        </div>


                     </div>
                    </div>
                </div>

            </>
        )
    }
}
export default Standing
