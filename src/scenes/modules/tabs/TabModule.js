import React, { Component } from 'react'
import { Tabs } from 'react-tabs'
import '../../../assets/tabs.css'

class TabModule extends Component {
	constructor(props) {
		super(props)

		this.state = {
			activeTab: 0
		}
	}

	setActiveTab(index) {
		this.setState({ activeTab: index })
	}

    render() {
        return (
            <div className="glassbg white-text">
                <Tabs selectedIndex={this.state.activeTab} onSelect={index => this.setActiveTab(index)}>
                    { this.props.children }
                </Tabs>
            </div>
        )
    }
}

export default TabModule