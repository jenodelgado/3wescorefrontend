import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import '../../assets/components/lineupss.css'
import '../../assets/themeSelector.css'

class Lineupss extends Component {

    constructor(props) {
        super(props)
     
    }

    render() {
            return(
            <>
            <div className="mobile">
                <div className="container" >    
                    <div className="table mt-20">
                        <div className="heads">
                            <div className="cols">
                                <a href="#!" className="lm-sx pointer">
                                <img src="sample1.png" alt="Menchester City" title="Manchester City" className="w-2"  />
                                </a>
                            
                            <div className="desc lm-sx">
                                <a href="#!" className="names colors ps-15 pointer">
                                    Manchester City
                                </a>
                            
                            <div className="sub ms-10 tm-1">
                                <span className="color-blow">Guardiola</span>
                                <span className="coloring lm-sx"></span>
                                </div>
                            </div>
                            </div>
                            <div className="cols">
                                <a href="#!" className="lm-sx pointer">
                                <img src="lyon.png" alt="Menchester City" title="Manchester City" className="w-2"  />
                                </a>
                            
                            <div className="desc lm-sx">
                                <a href="#!" className="names colors ps-15 pointer">
                                    Lyon
                                </a>
                            <div className="subs ms-10 tm-1">
                                <span className="color-blow">Rudy Garcia</span>
                                <span className="coloring lm-sx"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="cols ms-10 prap plop">
                            <div className="sky flakes align-aper">
                                <div className="colored shirtNumba">
                                    8
                                </div>
                                <a href="#!" className="lm-sx pointer">
                                    <img src="popot.png" alt="H.Aouar" title="H. Aouar" className="br-50 player-logo"></img>
                                </a>
                                <div className="desc lm-sx">
                                    <a href="#!" className="names color-333 pointer">H. Aouar</a>
                                    <div className="subs tm-1">
                                        <span className="coloring">M</span>
                                    </div>
                                </div>
                                <div className="lm-sx sky momot align-aper">
                                    <div className="hala">
                                        <svg className="svg-icon wot hala">
                                            <use>
                                                    
                                            </use>
                                        </svg>
                                    </div>
                                        <div className="color-blow ms-10">
                                            54
                                        </div>
                                </div>
                                <div className="lm-sx sky momot align-aper">
                                    <div className="hala">
                                        <svg className="svg-icon wot hala">
                                            <use>
                                                    
                                            </use>
                                        </svg>
                                    </div>
                                        <div className="color-blow ms-10">
                                            74
                                        </div>
                                </div>
                                <div className="smack color-man text-centa" style={{background: "#5CB400"}} >
                                    <div data-v-ef210d6e>8.6</div>
                                </div>
                            </div>     
                        </div>   
                        <div className="cols ms-10 prap plop">
                            <div className="sky flakes align-aper">
                                <div className="colored shirtNumba">
                                    8
                                </div>
                                <a href="#!" className="lm-sx pointer">
                                    <img src="poldoks.png" alt="H.Aouar" title="H. Aouar" className="br-50 player-logo"></img>
                                </a>
                                <div className="desc lm-sx">
                                    <a href="#!" className="names color-333 pointer">H. Aouar</a>
                                    <div className="subs tm-1">
                                        <span className="coloring">M</span>
                                    </div>
                                </div>
                                <div className="lm-sx sky momot align-aper">
                                    <div className="hala">
                                        <svg className="svg-icon wot hala">
                                            <use>
                                                    
                                            </use>
                                        </svg>
                                    </div>
                                        <div className="color-blow ms-10">
                                            54
                                        </div>
                                </div>
                                <div className="lm-sx sky momot align-aper">
                                    <div className="hala">
                                        <svg className="svg-icon wot hala">
                                            <use>
                                                    
                                            </use>
                                        </svg>
                                    </div>
                                        <div className="color-blow ms-10">
                                            74
                                        </div>
                                </div>
                                <div className="smack color-man text-centa" style={{background: "#5CB400"}} >
                                    <div data-v-ef210d6e>8.6</div>
                                </div>
                            </div>     
                        </div>
                    </div>
                </div>
            </div>
            </div>

            // moblie view
            <div className="mobileview">
                <div className="container nopad">
                    <div className="match nopad">
                        <div className="table mt-20">
                            <div className="heads match nopad">
                                <div className="cols match nopad">
                                    <a href="#!" className="lm-sx pointer match nopad">
                                    <img src="sample1.png" alt="Menchester City" title="Manchester City" className="w-2"  />
                                    </a>
                                    <div className="desc lm-sx match nopad">
                                    <a href="#!" className="names colors ps-15 pointer match nopad xxsm">
                                        Manchester City
                                    </a>
                                    <div className="sub ms-10 tm-1 match nopad">
                                        <span className="color-blow match nopad xxsm">Guardiola</span>
                                        <span className="coloring lm-sx match nopad"></span>
                                    </div>
                                    </div>
                                </div>
                                <div className="cols match nopad">
                                    <a href="#!" className="lm-sx pointer match nopad">
                                    <img src="sample1.png" alt="Menchester City" title="Manchester City" className="w-2"  />
                                    </a>
                                    <div className="desc lm-sx match nopad">
                                    <a href="#!" className="names colors ps-15 pointer match nopad">
                                        Manchester City
                                    </a>
                                    <div className="sub ms-10 tm-1 match nopad">
                                        <span className="color-blow match nopad">Guardiola</span>
                                        <span className="coloring lm-sx match nopad"></span>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                        <div className="cols ms-10 prap plop match nopad">
                            <div className="sky flakes align-aper match nopad">
                                <div className="colored shirtNumba match nopad">
                                    8
                                </div>
                                <a href="#!" className="lm-sx pointer match nopad">
                                    <img src="popot.png" alt="H.Aouar" title="H. Aouar" className="br-50 player-logo"></img>
                                </a>
                                <div className="desc lm-sx match nopad">
                                    <a href="#!" className="names color-333 pointer match nopad">H. Aouar</a>
                                    <div className="subs tm-1 match nopad">
                                        <span className="coloring match nopad">M</span>
                                    </div>
                                </div>
                                <div className="lm-sx sky momot align-aper match nopad match nopad">
                                    <div className="hala match nopad">
                                        <svg className="svg-icon wot hala">
                                            <use>
                                                    
                                            </use>
                                        </svg>
                                    </div>
                                        <div className="color-blow ms-10 match nopad">
                                            54
                                        </div>
                                </div>
                                <div className="lm-sx sky momot align-aper match nopad">
                                    <div className="hala match nopad">
                                        <svg className="svg-icon wot hala match nopad">
                                            <use>
                                                    
                                            </use>
                                        </svg>
                                    </div>
                                        <div className="color-blow ms-10 match nopad">
                                            74
                                        </div>
                                </div>
                                <div className="smack color-man text-centa" style={{background: "#5CB400"}} >
                                    <div data-v-ef210d6e>8.6</div>
                                </div>
                            </div>     
                        </div>   
                        <div className="cols ms-10 prap plop match nopad">
                            <div className="sky flakes align-aper match nopad">
                                <div className="colored shirtNumba ">
                                    8
                                </div>
                                <a href="#!" className="lm-sx pointer match nopad">
                                    <img src="poldoks.png" alt="H.Aouar" title="H. Aouar" className="br-50 player-logo"></img>
                                </a>
                                <div className="desc lm-sx match nopad">
                                    <a href="#!" className="names color-333 pointer match nopad">H. Aouar</a>
                                    <div className="subs tm-1 match nopad">
                                        <span className="coloring match nopad">M</span>
                                    </div>
                                </div>
                                <div className="lm-sx sky momot align-aper match nopad">
                                    <div className="hala match nopad">
                                        <svg className="svg-icon wot hala match nopad">
                                            <use>
                                                    
                                            </use>
                                        </svg>
                                    </div>
                                        <div className="color-blow ms-10 match nopad">
                                            54
                                        </div>
                                </div>
                                <div className="lm-sx sky momot align-aper match nopad">
                                    <div className="hala match nopad">
                                        <svg className="svg-icon wot hala match nopad">
                                            <use>
                                                    
                                            </use>
                                        </svg>
                                    </div>
                                        <div className="color-blow ms-10 match nopad">
                                            74
                                        </div>
                                </div>
                                <div className="smack color-man text-centa" style={{background: "#5CB400"}} >
                                    <div data-v-ef210d6e>8.6</div>
                                </div>
                            </div>     
                        </div>
                    </div>
                        </div>
                    </div>
                </div>
            </div>
            
            </>
                
            )
        }
     
    }
    

export default Lineupss
