import React, { Component } from 'react'
import { Tabs, TabList, Tab, TabPanel } from 'react-tabs'
import '../../assets/tabs.css'
import Accordion from './accordion/Accordion'
import SinglePageTitle from './SinglePageTitle'
import Fields from './Field'


class TabModule extends Component {
	constructor(props) {
		super(props)

		this.state = {
			activeTab: 0
		}
	}

	setActiveTab(index) {
		this.setState({ activeTab: index })
	}

	render() {
		return (
      <div className="glassbg white-text">
			<Tabs selectedIndex={this.state.activeTab} onSelect={index => this.setActiveTab(index)}>
                <TabList>
	                <Tab>All</Tab>
	                <Tab><i className="fa fa-clock fa-2sm"/> Live</Tab>
	                <Tab>Finished</Tab>
	                <Tab>Scheduled</Tab>
	                <Tab>Favorites</Tab>
                </TabList>
                <TabPanel> <Accordion allowMultipleOpen>
				
        <div className="percountry" label="Premier League"  ><span className="flag-icon flag-icon-gr">dd</span>
          <p >
					<SinglePageTitle />
          </p>
        </div>
        <div className="percountry" label="A-League">
          <p>
						<SinglePageTitle />
          </p>
        </div>

		   <div className="percountry" label="VIC Premier League">
          <p>
						<SinglePageTitle />
          </p>
        </div>
		   <div className="percountry" label="Brisbane Premier League Reserves">
          <p>
						<SinglePageTitle />
          </p>
        </div>
		{/* Live matches */}
      </Accordion></TabPanel>
                <TabPanel>
				<center>
				<h4><i className="fa fa-clock fa-2s"/> Live Matches</h4>
				</center>
					<Accordion allowMultipleOpen>		
        <div className="percountry" label="English Premier League" >
          <p >
					<SinglePageTitle />
          </p>
        </div>
        <div className="percountry" label="Serie A">
          <p>
						<SinglePageTitle />
          </p>
        </div>
		      <div  className="percountry" label="Copa Del Rey"  >
          <p >
					<SinglePageTitle />
          </p>
        </div>
        <div className="percountry" label="SuperCopa">
          <p>
						<SinglePageTitle />
          </p>
        </div>
		{/* Finished matches */}
      </Accordion></TabPanel>
                <TabPanel><Accordion allowMultipleOpen>
        <div className="percountry" label="Victorian Premier League U2O" >
          <p >
					<SinglePageTitle />
          </p>
        </div>
        <div className="percountry" label="U21 Youth League">
          <p>
						<SinglePageTitle />
          </p>
        </div>
		    <div className="percountry" label="CGD">
          <p>
						<SinglePageTitle />
          </p>
        </div>
		    <div className="percountry" label="Premiera Division">
          <p>
						<SinglePageTitle />
          </p>
        </div>
		{/* Scheduled Matches */}
      </Accordion></TabPanel>
                <TabPanel><Accordion allowMultipleOpen>
        <div className="percountry" label="Premier Division">
          <p >
					<SinglePageTitle />
          </p>
        </div>
        <div className="percountry" label="League Division 1">
          <p>
						<SinglePageTitle />
						<SinglePageTitle />
						<SinglePageTitle />
						<SinglePageTitle />
          </p>
        </div>
		{/* Favorite Matches */}
      </Accordion>
	  </TabPanel>
      <TabPanel>
      <center>
				<img src="wala.png" />
				<h6>You haven't selected any game yet</h6>
      
			</center>
		
	  </TabPanel>
	 
             </Tabs>
			 
			 </div>
		)
	}
}

export default TabModule