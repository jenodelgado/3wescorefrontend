import React, { Component } from 'react'
import {Validator} from './Validator';
import InputField from './InputField';
import Button from './Button';
import '../../assets/components/modal.css';
import '../../assets/InputField.css';
import { connect } from 'react-redux'
import {LoginUser} from '../../services/reducers/AuthAction'




class LoginForm extends Component {

      constructor(props) {
        super(props);


        this.state = {
            username: '',
            password: '',
            submitted: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({
            [event.target.id]: event.target.value
        })
        // const { name, value } = e.target;
        // this.setState({ [name]: value });
    }

    handleSubmit(event) {
        event.preventDefault();
        console.log('ready to login')
        console.log(this.state)
        console.log("aw",this.props);
        this.props.LoginUser(this.state, this.props.history);
        // this.setState({ submitted: true });
        // const { username, password } = this.state;
        // if (username && password) {
        //     this.props.login(username, password);
        // }
    }

    render() {
        const {authResponse} = this.props;
        const { loggingIn } = this.props;
        const { username, password, submitted } = this.state;
        return (
            <>
            <div className="col-md-12 col-md-offset-3">
                <div className="desktop">
                    <center>
                    <i className="fa fa-user-circle fa-3x"></i>
                    <span className="login-user">Login</span>
                    <br></br>
                    <b>{authResponse!=null && authResponse!=""?authResponse:null}</b>

                    </center>
                </div>

                <div className="iphone5">
                    <center>
                    <i className="fa fa-user-circle fa"></i>
                    <span className="login-user"> Login</span>
                    <br></br>
                    <b>{authResponse!=null && authResponse!=""?authResponse:null}</b>
                    </center>
                </div>
                <form name="form" onSubmit={this.handleSubmit}>
                    <div className={'form-group' + (submitted && !username ? ' has-error' : '')}>
                        <label className="label" htmlFor="username">Username</label>
                        <input type="text" 
                        className="form-control1" 
                        name="username" 
                        id="username"
                        onChange={this.handleChange} />
                        {submitted && !username &&
                            <div className="help-block">Username is required</div>
                        }
                    </div>
                    <div className={'form-group' + (submitted && !password ? ' has-error' : '')}>
                        <label className="label" htmlFor="password">Password</label>
                        <input type="password" 
                        className="form-control1" 
                        name="password"
                        id="password" 
                        onChange={this.handleChange} />
                        {submitted && !password &&
                            <div className="help-block">Password is required</div>
                        }
                        <br/>
                        <center>
                        <div className="button">
                        <button className="app-button">Login</button>
                        {loggingIn &&
                            <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
                        }
                        </div>
                        </center>
                        
                    </div>
                        
                </form>
            </div>
            </>
        );
    }
}

const mapStateToProps = (state) => {
    return{
        authResponse:state.auth.authResponse
    }
}

const mapDispatchToProps = (dispatch) =>{
    return{
        LoginUser:(creds, history) => dispatch(LoginUser(creds, history))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm)