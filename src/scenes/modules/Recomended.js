import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import '../../assets/components/recomended.css'
class Recomended extends Component {

 constructor(props) {
      super(props)
      this.state = {
         Matches: [
            { id: 1, homeLogo: 'test.png', awayLogo:'test.png',awayTeam:'Away Team',homeTeam:'Home Team' },
            { id: 2, homeLogo: 'test.png', awayLogo:'test.png',awayTeam:'Away Team',homeTeam:'Home Team' },
            { id: 3, homeLogo: 'test.png', awayLogo:'test.png',awayTeam:'Away Team',homeTeam:'Home Team' },
            { id: 4, homeLogo: 'test.png', awayLogo:'test.png',awayTeam:'Away Team',homeTeam:'Home Team' }
         ]
      }
   }

   renderTableData() {
      return this.state.Matches.map((match, index) => {
         const { id, homeLogo, homeTeam,  awayLogo, awayTeam } = match //destructuring
         return (
                <>
     
                    <div className="container">
                        <div className="matchupbox">
                        <tr key={id}>
                             
                                <div className="row">
                                  <div className="col-sm-1"> <td><img src='{homeLogo}'></img></td></div>
                                     <div className="col-sm-4"><td>{homeTeam}</td></div>               
                                          <div className="col-sm-1"><td >vs</td></div>
                                             <div className="col-sm-1"><td ><img src='{awayLogo}'></img></td></div>
                                         <div  className="col-sm-4"><td>{awayTeam}</td></div>      
                                <div className="col-sm-1"><td><Link to="/matchup"><i className ="fa fa-caret-square-o-right fa-2x"></i></Link></td></div>    
                                  
                              </div>
                         </tr>
                         </div>
                     </div>
           
              </>
         )
      })
   }

   render() {
      return (
         <div >
              <div className="box">
            <table id='Matches'style={{width:'inherit'}}>
               <tbody>
                  {this.renderTableData()}
                                 
               </tbody>
            </table>
            </div>
         </div>
      )
   }
}

export default Recomended