import React, { Component, Children } from "react";
import PropTypes from "prop-types";
import '../../../assets/components/accordion.css';
import AccordionSection from "./AccordionSection";

class Accordion extends Component {
  static propTypes = {
    allowMultipleOpen: PropTypes.bool,
    children: PropTypes.instanceOf(Object).isRequired
  };

  static defaultProps = {
    allowMultipleOpen: true
  };

  constructor(props) {
    super(props);

    const openSections = {};

    // Why is it contructed? This will not accept one child under accordion
    // this.props.children.forEach((child) => {
    //   if (child.props.isOpen) {
    //     openSections[child.props.label] = false;
    //   }
    // });

    this.state = { openSections };
  }

  onClick = (label) => {
    const {
      props: { allowMultipleOpen },
      state: { openSections }
    } = this;

    const isOpen = !!openSections[label];

    if (allowMultipleOpen) {
      this.setState({
        openSections: {
          ...openSections,
          [label]: !isOpen
        }
      });
    }
  };

  render() {
    const {
      onClick,
      props: { children },
      state: { openSections }
    } = this;

    let _children = Children.toArray(children)

    return (
      <div style={{ border: "#929292" }}>
        {_children.map((child) => (
          <AccordionSection 
            isOpen={!openSections[child.props.label]}
            onClick={ onClick }
            { ...child.props }
            
          >
            {child.props.children}
          </AccordionSection>
        ))}
      </div>
    );
  }
}

export default Accordion;
