/**
* @props containerClass
* @props contentClass
* @props labelIcon
* @props parentLabel
* @props label
*
* @author Joan Villariaza
*/
import React, { Component } from "react";
import PropTypes, { element } from "prop-types";
import axios from 'axios';
import '../../../assets/components/accordion.css'
import '../../../assets/themeSelector.css'
import jwt_decode from 'jwt-decode';

 

class AccordionSection extends Component {
  static propTypes = {
    children: PropTypes.instanceOf(Object).isRequired,
    isOpen: PropTypes.bool.isRequired,
    label: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
    labelIcon: PropTypes.string,
    parentLabel: PropTypes.string,
    caretClass: PropTypes.string,
    containerClass: PropTypes.string,
    contentClass: PropTypes.string,
  };

  onClick = () => {
    this.props.onClick(this.props.label);
  };

  onClickStar = () =>{
    let userID = localStorage.getItem("user");
    const user_id = jwt_decode(userID);
    const datafavs = this.props.label;
    const dataaa = { league:  datafavs , user_id: user_id.sub};
    axios.post('http://127.0.0.1:8000/api/favorite', dataaa)
    .then(response => this.setState({dataId: response.data.id}))
    .catch(error => {
      this.setState({ errorMessage: error.message });
      console.error('error Logs', error);
    });

  } 


  
  render() {
    const {
      onClick,
      onClickStar,
      props: { isOpen, label, labelIcon, parentLabel, caretClass, containerClass, contentClass }
    } = this;
    
    return (
      <>
      <div className="mobile">
      <div className={`accordion-container pt-2 ${containerClass}`}>
        <div onClick={onClick} style={{ cursor: "pointer" }} className={this.props.containerTitleClass}>
          { (labelIcon) ? (<span className={labelIcon}></span>) : '' } 
          <span className="gold-text">{ (parentLabel) ? `${parentLabel}: ` : '' }</span> 
          <span>{ label }</span>
          <div style={{ float: "right" }} className={caretClass}>
            {!isOpen && <span><i class="fas fa-caret-up" /></span>}
            {isOpen && <span><i class="fas fa-caret-down" /></span>}
          </div>
          <span style={{marginLeft: '5px'}} onClick={onClickStar} className="icon"><i className={`far fa-star`}></i></span>
        </div>
        <div></div>
        {isOpen && (
          <div className={`accordion-content px-2 ${contentClass}`}>
            { this.props.children }
          </div>
          
        )}
      </div>
      </div>
       {/* start for mobile view */}
          <div className="mobileview">
       <div className={`accordion-container pt- ${containerClass}`}>
        <div onClick={onClick} style={{ cursor: "pointer" }} className={this.props.containerTitleClass}>
          { (labelIcon) ? (<span className={labelIcon}></span>) : '' } 
          <span className="gold-text">{ (parentLabel) ? `${parentLabel}: ` : '' }</span> 
          <span>{ label }</span>
          <div style={{ float: "right" }} className={caretClass}>
            {!isOpen && <span><i class="fas fa-caret-up" /></span>}
            {isOpen && <span><i class="fas fa-caret-down" /></span>}
          </div>
        </div>

        {isOpen && (
          <div className={`accordion-content px-2 ${contentClass}`}>
            { this.props.children }
          </div>
          
        )}
      </div>
      </div>
      {/* end for mobile view */}
      </>
    );
  }
}

export default AccordionSection;
