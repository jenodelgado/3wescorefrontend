import React, { Component } from 'react'
import InputField from './InputField';
import Button from 'react-redux';
import '../../assets/components/modal.css';
import { connect } from 'react-redux'
import {signUp} from '../../services/reducers/AuthAction'
import '../../assets/InputField.css';




class RegistrationForm extends Component {
 constructor(props) {
        super(props);

        this.state = {
                firstname: '',
                lastname: '',
                username: '',
                email: '',
                password: '',
                submitted: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({
            [event.target.id]: event.target.value
        })

        // const { name, value } = event.target;
        // const { user } = this.state;
        // this.setState({
        //     user: {
        //         ...user,
        //         [name]: value
        //     }
        // });
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.signUp(this.state);


        this.setState({ submitted: true });
        const { firstname, lastname, username, email, password } = this.state;
        if (firstname && lastname && username && email && password) {
        }
    }

    render() {
        const {authResponse} = this.props;
        const { registering  } = this.props;
        const { firstname, lastname, username, email, password, cpassword, submitted } = this.state;
        return (
            <>
            <div className="col-md-12 col-md-offset-3">
               <div className="form-container">
               <center>
               <i className="mimi fa fa-user-plus fa-2x"></i>
                <span className="register-user"> Register</span>
                </center>
                <b>{authResponse!=null && authResponse!=""?authResponse:null}</b>

                <form name="form" onSubmit={this.handleSubmit}>
                    <div className={'form-group' + (submitted && !firstname ? ' has-error' : '')}>
                        <label className="label1" htmlFor="firstname">First Name</label>
                        <input type="text" 
                        className="form-control2"
                        name="firstname" 
                        id="firstname"
                        value={firstname}
                        onChange={this.handleChange} />
                        {submitted && !firstname &&
                            <div className="help-block">First Name is required</div>
                        }
                    </div>

                    <div className={'form-group' + (submitted && !lastname ? ' has-error' : '')}>
                        <label className="label1" htmlFor="lastname">Last Name</label>
                        <input type="text" 
                        className="form-control2" 
                        name="lastname" 
                        id="lastname"
                        value={lastname}
                        onChange={this.handleChange} />
                        {submitted && !lastname &&
                            <div className="help-block">Last Name is required</div>
                        }
                    </div>
                    <div className={'form-group' + (submitted && !username ? ' has-error' : '')}>
                        <label className="label1" htmlFor="username">Username</label>
                        <input type="text" 
                        className="form-control2" 
                        name="username" 
                        id="username"
                        onChange={this.handleChange} />
                        {submitted && !username &&
                            <div className="help-block">Username is required</div>
                        }
                    </div>
                    <div className={'form-group' + (submitted && !email ? ' has-error' : '')}>
                        <label className="label1" htmlFor="email">Email</label>
                        <input type="text" 
                        className="form-control2" 
                        name="email" 
                        id="email"
                        onChange={this.handleChange} />
                        {submitted && !email &&
                            <div className="help-block">Email is required</div>
                        }
                    </div>
                    <div className={'form-group' + (submitted && !password ? ' has-error' : '')}>
                        <label className="label1" htmlFor="password">Password</label>
                        <input type="password" 
                        className="form-control2" 
                        name="password" 
                        id="password"
                        onChange={this.handleChange} />
                        {submitted && !password &&
                            <div className="help-block">Password is required</div>
                        }
                    </div>
                      <div className={'form-group' + (submitted && !password!==cpassword ? ' has-error' : '')}>
                        <label className="label1" htmlFor="Confirm password">Confrim Password</label>
                        <input type="password" 
                        className="form-control2" 
                        name="confirm_password" 
                        id="confirm_password"
                        onChange={this.handleChange} />
                        {submitted && !password &&
                            <div className="help-block">Passwod did not match</div>
                        }
                        <br/>
                        <center>
                        <div className="button1">
                        <button className="app-button">Register</button>
                        {registering && 
                            <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
                        }
                    </div>
                    </center>
                    </div>
                    
                </form>
               </div>
            </div>
            </>
        );
    }
}

const mapStateToProps = (state) => {
    return{
        authResponse:state.auth.authResponse
    }
}

const mapDispatchToProps = (dispatch) =>{
    return{
        signUp:(creds) => dispatch(signUp(creds))
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(RegistrationForm)