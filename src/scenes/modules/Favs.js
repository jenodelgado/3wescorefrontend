import React, { useEffect, useState } from 'react';
import axios from 'axios'


export default class PersonList extends React.Component {
    state = {
        title: []
    }

    componentDidMount() {
        axios.get(`http://127.0.0.1:8000/api/favorite`)
        .then(res => {
            const title = res.data;
            this.setState({ title });
        })
    }

    render() {
        return (
        <ul>
            { this.state.title.map(title => <li>{title.league}</li>)}
        </ul>
        )
    }
}

