import React, { Component } from 'react'
import RenderIf from 'render-if'
import '../../assets/components/breadcrumb.css'

class Breadcrumb extends Component{
    constructor(props){
        super(props)
    }

    designBreadCrumbs() {
        const { links } = this.props
        if (!links)
            return ''
        
        return links.map(_link => {
            if (!_link.link) {
                return (<li><span className="white-text">{_link.name}</span></li>)
            } else {
                return (<li><a href={_link.link} className="link">{_link.name}</a></li>)
            }
        })
    }

    render() {
        return (
           <div >
                <div className="container">
                    <div className="breadcrumb-box">
                        <ul className="breadcrumb">
                            {this.designBreadCrumbs()}
                        </ul>
                    </div>
              </div>
           </div>
        )
    }
}
export default Breadcrumb