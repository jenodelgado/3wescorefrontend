import React, { Component } from 'react'
import ReactPlayer from "react-player";
import '../../assets/components/video.css'

class Video extends Component {

   

   render() {
      return (
         <>
         <div className="container" style={{paddingLeft:'0px'}}>
         <div className="box">
             <div className="matchup">
            <div className="row">   
                   <div className="col-sm-3 left-text">
                      <label className="text-label-left"> Home Team</label>
                   </div>
                       <div className="col-sm-1 left-text">
                     <i class="fab fa-battle-net fa-2x"></i>
                     </div>
                   
                   <div className="col-sm-4 center-text">
                      <label>Game Time</label>
                   </div>
                       <div className="col-sm-1 right-text" >
                     <i class="fas fa-biohazard fa-2x"></i>
                   </div>
                   <div className="col-sm-3 right-text" >
                      <label className="text-label-right">Away Team</label>
                   </div>
             
               <ReactPlayer className="videocss"
                 width='900px'
                 height='463px'
        url="https://www.youtube.com/watch?v=xSsiS304iY8"
      />
      
            </div>
        </div>
        </div>
         </div>
         </>
      );
   }
}

export default Video