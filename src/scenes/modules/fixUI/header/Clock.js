import React, { Component } from 'react'
import Clock from 'react-live-clock';
import 'semantic-ui-css/semantic.min.css'
import { Dropdown } from 'semantic-ui-react'
import 'moment-timezone';
import './Clock.css'


const options = [
    {
        "key": "Etc/GMT-12",
        "text": "Etc/GMT-12",
        "value": "Etc/GMT-12"
      },
      {
        "key": "Etc/GMT-11",
        "text": "Etc/GMT-11",
        "value": "Etc/GMT-11"
      },
      {
        "key": "Pacific/Midway",
        "text": "Pacific/Midway",
        "value": "Pacific/Midway"
      },
      {
        "key": "America/Adak",
        "text": "America/Adak",
        "value": "America/Adak"
      },
      {
        "key": "America/Anchorage",
        "text": "America/Anchorage",
        "value": "America/Anchorage"
      },
      {
        "key": "Pacific/Gambier",
        "text": "Pacific/Gambier",
        "value": "Pacific/Gambier"
      },
      {
        "key": "America/Dawson_Creek",
        "text": "America/Dawson_Creek",
        "value": "America/Dawson_Creek"
      },
      {
        "key": "America/Ensenada",
        "text": "America/Ensenada",
        "value": "America/Ensenada"
      },
      {
        "key": "America/Los_Angeles",
        "text": "America/Los_Angeles",
        "value": "America/Los_Angeles"
      },
      {
        "key": "America/Chihuahua",
        "text": "America/Chihuahua",
        "value": "America/Chihuahua"
      },
      {
        "key": "America/Denver",
        "text": "America/Denver",
        "value": "America/Denver"
      },
      {
        "key": "America/Belize",
        "text": "America/Belize",
        "value": "America/Belize"
      },
      {
        "key": "America/Cancun",
        "text": "America/Cancun",
        "value": "America/Cancun"
      },
      {
        "key": "America/Chicago",
        "text": "America/Chicago",
        "value": "America/Chicago"
      },
      {
        "key": "Chile/EasterIsland",
        "text": "Chile/EasterIsland",
        "value": "Chile/EasterIsland"
      },
      {
        "key": "America/Bogota",
        "text": "America/Bogota",
        "value": "America/Bogota"
      },
      {
        "key": "America/Havana",
        "text": "America/Havana",
        "value": "America/Havana"
      },
      {
        "key": "America/New_York",
        "text": "America/New_York",
        "value": "America/New_York"
      },
      {
        "key": "America/Caracas",
        "text": "America/Caracas",
        "value": "America/Caracas"
      },
      {
        "key": "America/Campo_Grande",
        "text": "America/Campo_Grande",
        "value": "America/Campo_Grande"
      },
      {
        "key": "America/Glace_Bay",
        "text": "America/Glace_Bay",
        "value": "America/Glace_Bay"
      },
      {
        "key": "America/Goose_Bay",
        "text": "America/Goose_Bay",
        "value": "America/Goose_Bay"
      },
      {
        "key": "America/Santiago",
        "text": "America/Santiago",
        "value": "America/Santiago"
      },
      {
        "key": "America/La_Paz",
        "text": "America/La_Paz",
        "value": "America/La_Paz"
      },
      {
        "key": "America/Argentina/Buenos_Aires",
        "text": "America/Argentina/Buenos_Aires",
        "value": "America/Argentina/Buenos_Aires"
      },
      {
        "key": "America/Montevideo",
        "text": "America/Montevideo",
        "value": "America/Montevideo"
      },
      {
        "key": "America/Araguaina",
        "text": "America/Araguaina",
        "value": "America/Araguaina"
      },
      {
        "key": "America/Godthab",
        "text": "America/Godthab",
        "value": "America/Godthab"
      },
      {
        "key": "America/Miquelon",
        "text": "America/Miquelon",
        "value": "America/Miquelon"
      },
      {
        "key": "America/Sao_Paulo",
        "text": "America/Sao_Paulo",
        "value": "America/Sao_Paulo"
      },
      {
        "key": "America/St_Johns",
        "text": "America/St_Johns",
        "value": "America/St_Johns"
      },
      {
        "key": "America/Noronha",
        "text": "America/Noronha",
        "value": "America/Noronha"
      },
      {
        "key": "Atlantic/Cape_Verde",
        "text": "Atlantic/Cape_Verde",
        "value": "Atlantic/Cape_Verde"
      },
      {
        "key": "Europe/Belfast",
        "text": "Europe/Belfast",
        "value": "Europe/Belfast"
      },
      {
        "key": "Africa/Abidjan",
        "text": "Africa/Abidjan",
        "value": "Africa/Abidjan"
      },
      {
        "key": "Europe/Dublin",
        "text": "Europe/Dublin",
        "value": "Europe/Dublin"
      },
      {
        "key": "Europe/Lisbon",
        "text": "Europe/Lisbon",
        "value": "Europe/Lisbon"
      },
      {
        "key": "Europe/London",
        "text": "Europe/London",
        "value": "Europe/London"
      },
      {
        "key": "UTC",
        "text": "UTC",
        "value": "UTC"
      },
      {
        "key": "Africa/Algiers",
        "text": "Africa/Algiers",
        "value": "Africa/Algiers"
      },
      {
        "key": "Africa/Windhoek",
        "text": "Africa/Windhoek",
        "value": "Africa/Windhoek"
      },
      {
        "key": "Atlantic/Azores",
        "text": "Atlantic/Azores",
        "value": "Atlantic/Azores"
      },
      {
        "key": "Atlantic/Stanley",
        "text": "Atlantic/Stanley",
        "value": "Atlantic/Stanley"
      },
      {
        "key": "Europe/Amsterdam",
        "text": "Europe/Amsterdam",
        "value": "Europe/Amsterdam"
      },
      {
        "key": "Europe/Belgrade",
        "text": "Europe/Belgrade",
        "value": "Europe/Belgrade"
      },
      {
        "key": "Europe/Brussels",
        "text": "Europe/Brussels",
        "value": "Europe/Brussels"
      },
      {
        "key": "Africa/Cairo",
        "text": "Africa/Cairo",
        "value": "Africa/Cairo"
      },
      {
        "key": "Africa/Blantyre",
        "text": "Africa/Blantyre",
        "value": "Africa/Blantyre"
      },
      {
        "key": "Asia/Beirut",
        "text": "Asia/Beirut",
        "value": "Asia/Beirut"
      },
      {
        "key": "Asia/Damascus",
        "text": "Asia/Damascus",
        "value": "Asia/Damascus"
      },
      {
        "key": "Asia/Gaza",
        "text": "Asia/Gaza",
        "value": "Asia/Gaza"
      },
      {
        "key": "Asia/Jerusalem",
        "text": "Asia/Jerusalem",
        "value": "Asia/Jerusalem",
      },
      {
        "key": "Africa/Addis_Ababa",
        "text": "Africa/Addis_Ababa",
        "value": "Africa/Addis_Ababa"
      },
      {
        "key": "Asia/Riyadh89",
        "text": "Asia/Riyadh89",
        "value": "Asia/Riyadh89"
      },
      {
        "key": "Europe/Minsk",
        "text": "Europe/Minsk",
        "value": "Europe/Minsk"
      },
      {
        "key": "Asia/Tehran",
        "text": "Asia/Tehran",
        "value": "Asia/Tehran"
      },
      {
        "key": "Asia/Dubai",
        "text": "Asia/Dubai",
        "value": "Asia/Dubai"
      },
      {
        "key": "Asia/Yerevan",
        "text": "Asia/Yerevan",
        "value": "Asia/Yerevan"
      },
      {
        "key": "Europe/Moscow",
        "text": "Europe/Moscow",
        "value": "Europe/Moscow"
      },
      {
        "key": "Asia/Kabul",
        "text": "Asia/Kabul",
        "value": "Asia/Kabul"
      },
      {
        "key": "Asia/Tashkent",
        "text": "Asia/Tashkent",
        "value": "Asia/Tashkent"
      },
      {
        "key": "Asia/Kolkata",
        "text": "Asia/Kolkata",
        "value": "Asia/Kolkata"
      },
      {
        "key": "Asia/Katmandu",
        "text": "Asia/Katmandu",
        "value": "Asia/Katmandu"
      },
      {
        "key": "Asia/Dhaka",
        "text": "Asia/Dhaka",
        "value": "Asia/Dhaka"
      },
      {
        "key": "Asia/Yekaterinburg",
        "text": "Asia/Yekaterinburg",
        "value": "Asia/Yekaterinburg"
      },
      {
        "key": "Asia/Rangoon",
        "text": "Asia/Rangoon",
        "value": "Asia/Rangoon"
      },
      {
        "key": "Asia/Bangkok",
        "text": "Asia/Bangkok",
        "value": "Asia/Bangkok"
      },
      {
        "key": "Asia/Novosibirsk",
        "text": "Asia/Novosibirsk",
        "value": "Asia/Novosibirsk"
      },
      {
        "key": "Etc/GMT+8",
        "text": "Etc/GMT+8",
        "value": "Etc/GMT+8"
      },
      {
        "key": "Asia/Hong_Kong",
        "text": "Asia/Hong_Kong",
        "value": "Asia/Hong_Kong"
      },
      {
        "key": "Asia/Krasnoyarsk",
        "text": "Asia/Krasnoyarsk",
        "value": "Asia/Krasnoyarsk"
      },
      {
        "key": "Australia/Perth",
        "text": "Australia/Perth",
        "value": "Australia/Perth"
      },
      {
        "key": "Australia/Eucla",
        "text": "Australia/Eucla",
        "value": "Australia/Eucla"
      },
      {
        "key": "Asia/Irkutsk",
        "text": "Asia/Irkutsk",
        "value": "Asia/Irkutsk"
      },
      {
        "key": "Asia/Seoul",
        "text": "Asia/Seoul",
        "value": "Asia/Seoul"
      },
      {
        "key": "Asia/Tokyo",
        "text": "Asia/Tokyo",
        "value": "Asia/Tokyo"
      },
      {
        "key": "Australia/Adelaide",
        "text": "Australia/Adelaide",
        "value": "Australia/Adelaide"
      },
      {
        "key": "Australia/Darwin",
        "text": "Australia/Darwin",
        "value": "Australia/Darwin"
      },
      {
        "key": "Pacific/Marquesas",
        "text": "Pacific/Marquesas",
        "value": "Pacific/Marquesas"
      },
      {
        "key": "Etc/GMT+10",
        "text": "Etc/GMT+10",
        "value": "Etc/GMT+10"
      },
      {
        "key": "Australia/Brisbane",
        "text": "Australia/Brisbane",
        "value": "Australia/Brisbane"
      },
      {
        "key": "Australia/Hobart",
        "text": "Australia/Hobart",
        "value": "Australia/Hobart"
      },
      {
        "key": "Asia/Yakutsk",
        "text": "Asia/Yakutsk",
        "value": "Asia/Yakutsk"
      },
      {
        "key": "Australia/Lord_Howe",
        "text": "Australia/Lord_Howe",
        "value": "Australia/Lord_Howe"
      },
      {
        "key": "Asia/Vladivostok",
        "text": "Asia/Vladivostok",
        "value": "Asia/Vladivostok"
      },
      {
        "key": "Pacific/Norfolk",
        "text": "Pacific/Norfolk",
        "value": "Pacific/Norfolk"
      },
      {
        "key": "Etc/GMT+12",
        "text": "Etc/GMT+12",
        "value": "Etc/GMT+12"
      },
      {
        "key": "Asia/Anadyr",
        "text": "Asia/Anadyr",
        "value": "Asia/Anadyr"
      },
      {
        "key": "Asia/Magadan",
        "text": "Asia/Magadan",
        "value": "Asia/Magadan"
      },
      {
        "key": "Pacific/Auckland",
        "text": "Pacific/Auckland",
        "value": "Pacific/Auckland"
      },
      {
        "key": "Pacific/Chatham",
        "text": "Pacific/Chatham",
        "value": "Pacific/Chatham"
      },
      {
        "key": "Pacific/Tongatapu",
        "text": "Pacific/Tongatapu",
        "value": "Pacific/Tongatapu"
      },
      {
        "key": "Pacific/Kiritimati",
        "text": "Pacific/Kiritimati",
        "value": "Pacific/Kiritimati"
      }

    
  ]
  

class Time extends Component {
    state = { options }
    
    
    constructor(props) {
        super(props);

        this.state = {
           timezone: ''
        };

        this.handleChange = this.handleChange.bind(this)
        
    }

    handleChange (e, { value }) {
        this.setState({ timezone: value })   
    }

    componentWillMount () {
        fetch('https://ipapi.co/json/')
        .then( res => res.json())
        .then(response => {
            this.setState({
                timezone: response.timezone
            })
        })
        .catch((data, status) => {
            console.log('Request failed');
        })
    }

    render() {
        const { currentValue } = this.state
        return (
            <div>
                <Clock format={'dddd - MMMM DD, yyyy, h:mm:ss A'} ticking={true} timezone={this.state.timezone} />
                <span>&nbsp;</span>
                <span> | </span>
                <span>&nbsp;</span>

                <Dropdown classvalue="medium"
                options={options}
                search
                value={currentValue}
                placeholder="Timezone"
                onChange={this.handleChange}
                />
            </div>
        )
    }
}





export default Time
