import 'semantic-ui-css/semantic.min.css'
import { Dropdown, Label, Icon } from 'semantic-ui-react'
import React, { Component } from 'react'

const options = [
    { key: 'English', text: 'English', value: 'English' },
    { key: 'French', text: 'French', value: 'French' },
    { key: 'Spanish', text: 'Spanish', value: 'Spanish' },
    { key: 'German', text: 'German', value: 'German' },
    { key: 'Chinese', text: 'Chinese', value: 'Chinese' },
  ]
  
  
  class AutoComplete extends Component {
    state = { options }
  
   
  
    handleChange = (e, { value }) => this.setState({ currentValue: value })
  
    render() {
      const { currentValue } = this.state
  
      return (
       <>
        <Dropdown className="medium"
          options={this.state.options}
          search
          value={currentValue}
          placeholder="Timezone"
          onChange={this.handleChange}
        />
       </>
      )
    }
  }
  
  export default AutoComplete
  