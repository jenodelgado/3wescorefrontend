import styled from 'styled-components';
import { NavLink as Link} from 'react-router-dom';
import {FaBars} from 'react-icons/fa';

export const Nav = styled.nav`
  width: 100%;
  background: #343434;
  height: auto;
  display: flex;
  justify-content: space-between;
  / Third Nav /
  / justify-content: flex-start; /
`;

export const Navs = styled.nav`
  width: auto;
  background: #343434;
  height: auto;
  display: flex;
  justify-content: space-between;
  / Third Nav /
  / justify-content: flex-start; /
  @media screen and (max-width: 768px) {
    display: none !important;
  }
`;

export const Navss = styled.nav`
  width: auto;
  background: #343434;
  height: auto;
  display: flex;
  justify-content: space-between;
  / Third Nav /
  / justify-content: flex-start; /
 
`;

export const Desktop = styled.nav`
  @media screen and (max-width: 768px) {
    display: none !important;
  }
`;


export const Nav2 = styled.nav`
  background: #dbb963;
  height: 46px;
  display: flex;
  justify-content: space-between;
  / Third Nav /
  / justify-content: flex-start; /
`;

export const Nav3 = styled.nav`
  width: 100%;
  background: #6c757d;
  height: 79px;
  display: flex;
  justify-content: space-between;
  / Third Nav /
  / justify-content: flex-start; /
  @media screen and (max-width: 768px) {
    .realtime-clocks {
      font-size: 8 !important;
    }
  }
`;

export const NavLink = styled(Link)`
  color: #fff;
  display: flex;
  align-items: center;
  text-decoration: none;
  // padding: 0 1rem;
  height: 100%;
  cursor: pointer;
  text-shadow: 0px 4px 3px rgba(0, 0, 0, 0.69);
  &.active {
    color: #dbb963;
  }
`;

export const NavBtnLink2 = styled(Link)`
border-radius: 4px;
padding: 10px 22px;
color: #black;
outline: none;
border: none;
cursor: pointer;
transition: all 0.2s ease-in-out;
text-decoration: none;
/ Second Nav /
margin-left: 24px;
font-weight: 500;
/* identical to box height */


color: #FFFFFF;

text-shadow: 0px 1px 3px #000000;
&:hover {
  transition: all 0.2s ease-in-out;
  color: #000000
}
`;

export const Bars = styled(FaBars)`
    display: none;
    color: #fff;
    
    @media screen and (max-width: 768px) {
    display: block;
    position: absolute;
    top: 0;
    right: 0;
    transform: translate(-100%, 75%);
    font-size: 1.8rem;
    cursor: pointer;
  }
`;

export const NavMenu = styled.div`
  display: flex;
  align-items: center;
  margin-right: -24px;
  float: right;
  / Second Nav /
  / margin-right: 24px; /
  / Third Nav /
  /* width: 100vw;
  white-space: nowrap; */
  @media screen and (max-width: 768px) {
    display: none !important;
  }
`;



export const NavMenu2 = styled.div`
  display: flex;
  margin-right: 50px;
  margin-top: 10px;
  align-items: center;
  margin-right: 18px;
  float: right;
  font-size: 25px;
  / Second Nav /
  / margin-right: 24px; /
  / Third Nav /
  /* width: 100vw;
  white-space: nowrap; */
  @media screen and (max-width: 768px) {
    display: none !important;
  }

  @media screen and (max-width: 360px) {

    .mobile-fas{
      font-size: 11px !important;
      display: inline-block !important;
    }

  }

  @media screen and (max-width: 375px) {

    .mobile-fas{
      font-size: 16px;
      display: inline-block !important;
    }

  }

  @media screen and (max-width: 414px) {
    font-size: 16px;
  }
`;

export const NavBtn = styled.nav`
  display: flex;
  align-items: center;
  margin-right: 10px;
  / Third Nav /
  /* justify-content: flex-end;
  width: 100vw; */
  @media screen and (max-width: 768px) {
    display: none;
  }
`;

export const NavBtnLink = styled(Link)`
  border-radius: 4px;
  padding: 10px 15px;
  color: #fff;
  outline: none;
  border: none;
  cursor: pointer;
  transition: all 0.2s ease-in-out;
  text-decoration: none;
  / Second Nav /
  margin-left: 24px;
  &:hover {
    transition: all 0.2s ease-in-out;
    color: #dbb963;
  }
`;

export const TabList = styled.nav`
    border-bottom: 1px solid #aaa;
    margin: 0 0 10px;
    padding: 0;
    text-align: left;
`;

export const Tab = styled.nav`
    display: inline-block;
    border: 1px solid transparent;
    border-bottom: none;
    bottom: -1px;
    position: relative;
    list-style: none;
    padding: 6px 12px;
    cursor: pointer;
    font-size: 10pt;
`;

export const colomboy = styled.nav`
   padding-left: 120px;
`;

