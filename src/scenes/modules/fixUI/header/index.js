import React, { Component, useState }  from 'react';
import Modal from 'react-modal';
import PropTypes from "prop-types";
import { Nav, Navs, NavLink, Bars, NavMenu, NavBtn, NavBtnLink, Nav2, Nav3, NavMenu2, Desktop, Navss } from './NavbarElements';
import { Tabs, TabList, Tab, TabPanel } from 'react-tabs'
import 'bootstrap/dist/css/bootstrap.min.css';
import 'antd/dist/antd.css';
import Time from './Clock'
import MobileTime from './MobileClock'
import SinglePageTitle from '../../SinglePageTitle'
import { connect } from 'react-redux'

import {
    MDBDropdown, MDBDropdownToggle, MDBDropdownMenu, MDBDropdownItem
    } from "mdbreact";

import '../../../../assets/tabs.css';
import '../../../../assets/modal.css';
import '../../../../assets/themeSelector.css';
import LoginForm from '../../../modules/LoginForm'
import RegistrationForm from '../../../modules/RegistrationForm'
import { Link } from 'react-router-dom';



class Navbar extends Component {
    constructor(props) {
        super(props)

        this.state = {
            isOpen: false,
            showLoginModal: false,
            showRegisterModal: false,
            mobileMenuClass: false,
            leagues: this.props.leagues
        }

        this.toggleMobileMenus = this.toggleMobileMenus.bind(this)
        this.backToHome = this.backToHome.bind(this)
    }
    componentWillReceiveProps(nextProps) {
		this.setState({ leagues: nextProps.leagues })
	}

    toggleCollapse() {
        this.setState({isOpen: !this.state.isOpen})
    }

    toggleLoginModal(bool) {
        this.setState({ showLoginModal: bool })
    }

    toggleRegisterModal(bool) {
        this.setState({ showRegisterModal: bool })
    }

    toggleMobileMenus() {

        this.setState({ mobileMenuClass: !this.state.mobileMenuClass })
        // this.setState({ mobileMenuClass: 'd-none d-sm-block d-md-none' })
        // this.setState({isOpen: !this.state.isOpen})
    }

    toggleMobileChina() {
        this.setState({ mobileChinese: !this.state.mobileChinese })
     }

    backToHome() {
        this.props.history.push('/')
    }

    render() {
        const { showLoginModal, showRegisterModal } = this.state
        const { selectSportTheme, sportIndex } = this.props

        return (<>
            <Nav>
                 <Bars onClick={this.toggleMobileMenus}onClick={this.toggleMobileMenus}/>
                <div className="container mt-3">
                    <div className="col d-none d-md-block">
                        {this.generateMenu()}
                    </div>
                    <div className="row">
                        <span onClick={this.backToHome}>
                            <img className="main-logo" src="/logo.png" alt="logo" />
                        </span>
                    </div>
                </div>
            </Nav>
            <Desktop>
                <Navs>
                    
                    <Bars onClick={this.toggleMobileMenus}/>
                    <div className="container">
                            <h6 className="realtime-clock"><Time /></h6>
                    </div>
                </Navs>
            </Desktop>
            {
                this.state.mobileMenuClass?
                <>
                    <Nav3>
                        <div className="container">
                            <div className="">
                                {this.generateMenus()}
                                <center>
                                    <h6 className="mobileTime"><MobileTime /></h6>
                                </center>
                            </div>
                        </div>
                    </Nav3>
                 </>   
                 :null
            }
            
            <Nav2>  
                <div className="container main-tabs margin-l5">
                    <Tabs selectedIndex={!sportIndex ? 0 : sportIndex}>
                        <TabList>
                            <Tab onClick={ _ => selectSportTheme('FOOTBALL', 0) } ><i class="fas fa-futbol"/><span className="d-none d-sm-none d-md-inline">&nbsp; Football</span></Tab>
                            <Tab onClick={ _ => selectSportTheme('BASKETBALL', 1) } ><i class="fas fa-basketball-ball"/><span className="d-none d-sm-none d-md-inline">&nbsp; Basketball</span></Tab>
                            <Tab onClick={ _ => selectSportTheme('HORSERACING', 2) } ><i class="fas fa-horse-head"/><span className="d-none d-sm-none d-md-inline">&nbsp; Horse Racing</span></Tab>
                            <Tab onClick={ _ => selectSportTheme('ESPORTS', 3) } ><i class="fab fa-steam"/> <span className="d-none d-sm-none d-md-inline">&nbsp; E-Sports </span></Tab >
                        </TabList>
                    </Tabs>     
                </div>
            </Nav2>
         
            <Modal className='centered1' isOpen={ showLoginModal }>
                <button to='/login' onClick={() => this.toggleLoginModal(false)} className="btnx">x</button>
                <LoginForm {...this.props} />  
            </Modal>
            <Modal className='centered2' isOpen={ showRegisterModal }>
                <button to='/register' onClick={() => this.toggleRegisterModal(false)} className="btnx1">x</button>
                <RegistrationForm />
            </Modal>
        </>)
    }

    generateMenu(){
        const { setLanguage } = this.props
        return (<NavMenu>
            <MDBDropdown>
                <MDBDropdownToggle nav caret>
                    <span className="mr-2"><i className="fa fa-globe margin-r5" /> English </span>
                </MDBDropdownToggle>
                <MDBDropdownMenu onClick={() => this.toggleCollapse(true)}>
                    <MDBDropdownItem onClick={_ => setLanguage('')} >English</MDBDropdownItem>
                    <MDBDropdownItem onClick={_ => setLanguage('CH')}>Chinese</MDBDropdownItem>
                    <MDBDropdownItem onClick={_ => setLanguage('TH')}>Thailand</MDBDropdownItem>
                </MDBDropdownMenu>
            </MDBDropdown>

            <NavBtnLink to='/' onClick={() => this.toggleLoginModal(true)}>
                <span><i className="fas fa-user-lock margin-r5" /> Login</span>
            </NavBtnLink>
            
            <NavBtnLink to='/' onClick={() => this.toggleRegisterModal(true)}>
                <span><i className="fas fa-user-plus margin-r5" /> Register</span>
            </NavBtnLink> 
        </NavMenu>)
    }
    generateMenus(){
    const {setLanguage} = this.props
    return (        <NavMenu2>
                        <MDBDropdown>
                            <MDBDropdownToggle nav caret>
                            <i className="fas fa-globe margin-r5 mobile-fa" /><span className="mobile-fas"> English </span>
                            </MDBDropdownToggle>
                            <MDBDropdownMenu onClick={() => this.toggleCollapse(true)}>
                                <MDBDropdownItem onClick={_ => setLanguage('')} >English</MDBDropdownItem>
                                <MDBDropdownItem onClick={_ => setLanguage('CH')}>Chinese</MDBDropdownItem>
                                <MDBDropdownItem onClick={_ => setLanguage('TH')}>Thailand</MDBDropdownItem>
                            </MDBDropdownMenu>
                        </MDBDropdown>
                        
                        <NavBtnLink to='/' onClick={() => this.toggleLoginModal(true)}>
                        <i className="fas fa-user-lock margin-r5 mobile-fa" /><span className="mobile-fas"> Login</span>
                        </NavBtnLink>

                        <NavBtnLink to='/' onClick={() => this.toggleRegisterModal(true)}>
                        <i className="fas fa-user-plus margin-r5 mobile-fa" /><span className="mobile-fas"> Register</span>
                        </NavBtnLink>  
                    </NavMenu2>)
    }

}
const mapStateToProps = (state, ownProps) => ({
	leagues: state.League
})

Navbar.propTypes = {
    selectSportTheme: PropTypes.func 
}


export default connect( mapStateToProps ) (Navbar)
