import React from 'react';
import '../../../assets/components/footer.css'
import { Link } from 'react-router-dom'
import Accordion from '../../modules/accordion/Accordion'
import '../../../assets/themeSelector.css'


const Footer = () => {
    return (
    <>
    <div className="mobile">
        <div className="main-footer">
            <div className="container">
                <div className="row">

                    {/* Column1 */}
                    <div className="col-md-12">
                        <img className="main-logo mb-2" src="/logo.png"></img>
                        <ul className="list-unstyled">
                            <li>
                                3WeScore Football Livescore provides you with unparalleled football live scores and football results from over 2600+ football leagues, cups and tournaments. Get live scores, halftime and full time soccer results, goal scorers and assistants, cards, substitutions, match statistics and live stream from Premier League, La Liga, Serie A, Bundesliga, Ligue 1, Eredivisie, Russian Premier League, Brasileirão, MLS, Super Lig and Championship on 3WeScore.com.                              3WeScore offers to all the soccer fans live scores, soccer livescore, soccer scores, league tables and fixtures for leagues, cups and tournaments, and not only from the most popular football leagues as England Premier League, Spain La Liga, Italy Serie A, Germany Bundesliga, France Ligue 1, but also from a large range of football countries all over the world, including from North and South America, Asia and Africa. Our football livescore scorecards are updated live in real-time to keep you up to date with all the football match livescore updates happening today along with football livescore results for all finished football matches for every football & soccer league. On the match page, our football scorecards are allowing you to view past game results for all previously played fixtures for every football competitions. Get all of your football live results on 3WeScore.com!
                            </li>
                        </ul>
                    </div>

                    {/* Column3 */}
                    <hr/>
                    <div className="row">
                        <div className="col-md-4">
                            <h6>Next Match</h6>
                            <ul className="list-unstyled">
                               <li><a href="#!" target="_blank">Lazio - Torino</a></li>
                               <li><a href="#!" target="_blank">Borussia Monchengladbach - Borusssia Dortmund </a></li>
                               <li><a href="#!" target="_blank">Rot-Weiss Essen - Holstein Kiel</a></li>
                               <li><a href="#!" target="_blank">Jahn Regensburg - Werder Bremen</a></li>
                               <li><a href="#!" target="_blank">RB Leipzig VfL Wolfsburg</a></li>
                               <li><a href="#!" target="_blank">Juventus - Spezia</a></li>
                               <li><a href="#!" target="_blank">Manchester City-Wolves</a></li>
                               <li><a href="#!" target="_blank">Sassuolo - Napoli</a></li>
                               <li><a href="#!" target="_blank">Stade Brestois - Dijon</a></li>
                               <li><a href="#!" target="_blank">Lyon - Rennes</a></li>
                            </ul>
                        </div>
                        <div className="col-md-3">
                            <br/>
                            <ul className="list-unstyled">
                                <li><a href="#!" target="_blank">Metz - Angers</a></li>
                                <li><a href="#!" target="_blank">Nice - Nimes</a></li>
                                <li><a href="#!" target="_blank">Saint Etienne - Lens</a></li>
                                <li><a href="#!" target="_blank">Sheffield United - Aston Villa</a></li>
                                <li><a href="#!" target="_blank">Burnley - Leicester City</a></li>
                                <li><a href="#!" target="_blank">AC Milan - Udinese</a></li>
                                <li><a href="#!" target="_blank">Atlanta -Crotone</a></li>
                                <li><a href="#!" target="_blank">Benevento - Verona</a></li>
                                <li><a href="#!" target="_blank">Cagliari - Bologna</a></li>
                                <li><a href="#!" target="_blank">Fiorentina - AS Roma</a></li>
                            </ul>
                        </div>
                        <div className="col-md-3">
                        <h6>Football</h6>
                            <ul className="list-unstyled">
                                <li><a href="#!" target="_blank">Tournaments</a></li>
                                <li><a href="#!" target="_blank">Football Teams</a></li>
                                <li><a href="#!" target="_blank">Football Players</a></li>
                                <li><a href="#!" target="_blank">Head to head</a></li>
                                <li><a href="#!" target="_blank">England Premier League Schedule</a></li>
                                <li><a href="#!" target="_blank">Italian Serie A schedule</a></li>
                                <li><a href="#!" target="_blank">Spanish La Liga schedule</a></li>
                                <li><a href="#!" target="_blank">German Bundesliga schedule</a></li>
                                <li><a href="#!" target="_blank">France Ligue 1 schedule</a></li>
                                <li><a href="#!" target="_blank">FIFA Ranking</a></li>
                                <li><a href="#!" target="_blank">UEFA Ranking</a></li>
                            </ul>
                        </div>
                        <div className="col-md-2">
                        <h6>Basketball</h6>
                            <ul className="list-unstyled">
                                <li><a href="#!" target="_blank">Basketball Live Score</a></li>
                                <li><a href="#!" target="_blank">NBA Schedule</a></li>
                                <li><a href="#!" target="_blank">NBA Standings</a></li>
                                <li><a href="#!" target="_blank">NBA Teams</a></li>
                                <li><a href="#!" target="_blank">NBA Stats</a></li>
                                <li><a href="#!" target="_blank">NBA Live Score</a></li>
                                <li><a href="#!" target="_blank">CBA Schedule</a></li>
                                <li><a href="#!" target="_blank">CBA Standings</a></li>
                                <li><a href="#!" target="_blank">CBA Stats</a></li>
                                <li><a href="#!" target="_blank">CBA Live Score</a></li>
                            </ul>
                        </div>
                    </div>
                    <hr/>
                    <div className="row">
                        <div className="col-md-4">
                            <h6>Visit localized live score version of 3WeScore</h6>
                            <ul className="list-unstyled">
                               <li><a href="#!" target="_blank">Football Live Score</a></li>
                               <li><a href="#!" target="_blank">Futbol Canlı Skor</a></li>
                               <li><a href="#!" target="_blank">Score en direct Football</a></li>
                               <li><a href="#!" target="_blank">Ποδόσφαιρο Ζωντανά σκορ</a></li>
                            </ul>
                        </div>
                        <div className="col-md-3">
                            <br/>
                            <ul className="list-unstyled">
                                <li><a href="#!" target="_blank">ผล บอล สด</a></li>
                                <li><a href="#!" target="_blank">足球比分</a></li>
                                <li><a href="#!" target="_blank">Fußball Live Score</a></li>
                                <li><a href="#!" target="_blank">fotbal live výsledky</a></li>
                            </ul>
                        </div>
                        <div className="col-md-3">
                        <br/>
                            <ul className="list-unstyled">
                                <li><a href="#!" target="_blank">Futebol placar ao vivo</a></li>
                                <li><a href="#!" target="_blank">Skor Langsung Sepakbola</a></li>
                                <li><a href="#!" target="_blank">Футбол лайвскор</a></li>
                                <li><a href="#!" target="_blank">サッカーライブスコア</a></li>
                            </ul>
                        </div>
                        <div className="col-md-2">
                        <br/>
                            <ul className="list-unstyled">
                                <li><a href="#!" target="_blank">Fútbol en vivo</a></li>
                                <li><a href="#!" target="_blank">Nogomet rezultati uživo</a></li>
                                <li><a href="#!" target="_blank">Футболни Резултати на живо</a></li>
                                <li><a href="#!" target="_blank">축구 실시간 점수</a></li>
                            </ul>
                        </div>
                    </div>
                    <hr/>
                    <div className="row">
                        <center>
                        <p className="col-sm">
                            <a href="#!">
                                <img src="twitter.png" className="img-icons"></img>
                                <img src="facebook.png" className="img-icons"></img>
                                <img src="instagram.png" className="img-icons"></img>
                            </a>
                        </p>
                        <p className="col-sm">
                            <Link to='ContactUs'> Contact Us</Link> | <Link to='TermsOfService'>Terms of Service</Link> | <Link to='TermsOfService'> Privacy Policy</Link>
                        </p>
                        <p className="col-sm">
                            &copy;{new Date().getFullYear()} 3WeScore | 18+ Gamble Responsibly
                        </p>
                        <div className="line">-</div>
                        </center>
                    </div>
                </div>
            </div>
        </div>
        </div>

        {/*Mobile View*/}
        <div className="container nopad mobileviews">
        <div className="row nopad">
        <div className="col-md-12 mt-2">
                <Accordion allowMultipleOpen >
                        <div className="col-md-4 nopad" label="Next Match">
                            <ul className="list-unstyled" className="miming">
                               <li><a href="#!" target="_blank" className="coloris">Lazio - Torino</a></li>
                               <li><a href="#!" target="_blank">Borussia Monchengladbach - Borusssia Dortmund </a></li>
                               <li><a href="#!" target="_blank">Rot-Weiss Essen - Holstein Kiel</a></li>
                               <li><a href="#!" target="_blank">Jahn Regensburg - Werder Bremen</a></li>
                               <li><a href="#!" target="_blank">RB Leipzig VfL Wolfsburg</a></li>
                               <li><a href="#!" target="_blank">Juventus - Spezia</a></li>
                               <li><a href="#!" target="_blank">Manchester City-Wolves</a></li>
                               <li><a href="#!" target="_blank">Sassuolo - Napoli</a></li>
                               <li><a href="#!" target="_blank">Stade Brestois - Dijon</a></li>
                               <li><a href="#!" target="_blank">Lyon - Rennes</a></li>
                            </ul>
                        </div>
                </Accordion>
                <hr/>
                <Accordion allowMultipleOpen>
                        <div className="col-md-4" label="Football" className="miming">
                            <ul className="list-unstyled" className="miming">
                                <li><a href="#!" target="_blank">Tournaments</a></li>
                                <li><a href="#!" target="_blank">Football Teams</a></li>
                                <li><a href="#!" target="_blank">Football Players</a></li>
                                <li><a href="#!" target="_blank">Head to head</a></li>
                                <li><a href="#!" target="_blank">England Premier League Schedule</a></li>
                                <li><a href="#!" target="_blank">Italian Serie A schedule</a></li>
                                <li><a href="#!" target="_blank">Spanish La Liga schedule</a></li>
                                <li><a href="#!" target="_blank">German Bundesliga schedule</a></li>
                                <li><a href="#!" target="_blank">France Ligue 1 schedule</a></li>
                                <li><a href="#!" target="_blank">FIFA Ranking</a></li>
                                <li><a href="#!" target="_blank">UEFA Ranking</a></li>
                            </ul>
                        </div>
                </Accordion>
                <hr/>
                <Accordion allowMultipleOpen>
                        <div className="col-md-4" label="Basketball" className="miming">
                            <ul className="list-unstyled" className="miming">
                                <li><a href="#!" target="_blank">Basketball Live Score</a></li>
                                <li><a href="#!" target="_blank">NBA Schedule</a></li>
                                <li><a href="#!" target="_blank">NBA Standings</a></li>
                                <li><a href="#!" target="_blank">NBA Teams</a></li>
                                <li><a href="#!" target="_blank">NBA Stats</a></li>
                                <li><a href="#!" target="_blank">NBA Live Score</a></li>
                                <li><a href="#!" target="_blank">CBA Schedule</a></li>
                                <li><a href="#!" target="_blank">CBA Standings</a></li>
                                <li><a href="#!" target="_blank">CBA Stats</a></li>
                                <li><a href="#!" target="_blank">CBA Live Score</a></li>
                            </ul>
                        </div>
                </Accordion>
                <hr/>
                <Accordion allowMultipleOpen>
                        <div className="col-md-4 miming" label="Visit localized live score version of 3WeScore">
                            <ul className="list-unstyled" className="miming">
                               <li><a href="#!" target="_blank">Football Live Score</a></li>
                               <li><a href="#!" target="_blank">Futbol Canlı Skor</a></li>
                               <li><a href="#!" target="_blank">Score en direct Football</a></li>
                               <li><a href="#!" target="_blank">Ποδόσφαιρο Ζωντανά σκορ</a></li>
                               <li><a href="#!" target="_blank">ผล บอล สด</a></li>
                                <li><a href="#!" target="_blank">足球比分</a></li>
                                <li><a href="#!" target="_blank">Fußball Live Score</a></li>
                                <li><a href="#!" target="_blank">fotbal live výsledky</a></li>
                                <li><a href="#!" target="_blank">Futebol placar ao vivo</a></li>
                                <li><a href="#!" target="_blank">Skor Langsung Sepakbola</a></li>
                                <li><a href="#!" target="_blank">Футбол лайвскор</a></li>
                                <li><a href="#!" target="_blank">サッカーライブスコア</a></li>
                                <li><a href="#!" target="_blank">Fútbol en vivo</a></li>
                                <li><a href="#!" target="_blank">Nogomet rezultati uživo</a></li>
                                <li><a href="#!" target="_blank">Футболни Резултати на живо</a></li>
                                <li><a href="#!" target="_blank">축구 실시간 점수</a></li>
                            </ul>
                        </div>
                </Accordion>
                </div>
            </div>
        </div>
    </>
    )
}

export default Footer
