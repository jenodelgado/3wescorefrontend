import React, { Component } from 'react'

import '../../assets/themeSelector.css'
 class H2h extends Component {
    render() {
        return (
            <>
                    <div className="mobile">
              
                <div className="container">
                            <div className="match">
                                    
                                    <span className="match-update">Latest Matches</span>&nbsp;
                                    <span className="match-team">Chelsea</span>
                             </div>
                         <div class="div-table">
                                    <div class="trow">
                                        <div class="tcolumns tcolumn1">Leagues</div>
                                        <div class="tcolumns tcolumn2">Date</div>
                                        <div class="tcolumns tcolumn3">Home</div>
                                        <div class="tcolumns tcolumn4">Score</div>
                                        <div class="tcolumns tcolumn5">Away</div>
                                        <div class="tcolumns tcolumn6">Score Half</div>
                                        <div class="tcolumns tcolumn6">Result</div>
                                    </div>

                                    <div class="trow">
                                        <div class="tcolumne">English Premier League</div>
                                        <div class="tcolumn">2021/03/05</div>
                                        <div class="tcolumn">Liverpool</div>
                                        <div class="tcolumnes">0-1</div>
                                        <div class="tcolumn">Chelsea</div>
                                        <div class="tcolumn">0-1</div>
                                        <div class="tcolumn">
                                            <div className="tcolumnees">
                                                W
                                            </div>  
                                        </div>
                                    </div>


                                    <div class="trow">
                                        <div class="tcolumne">English Premier League</div>
                                        <div class="tcolumn">2021/03/05</div>
                                        <div class="tcolumn">Chelsea</div>
                                        <div class="tcolumnee">0-0</div>
                                        <div class="tcolumn">Manchester United</div>
                                        <div class="tcolumn">0-0</div>
                                        <div class="tcolumn">
                                            <div className="letter-D">
                                                D
                                            </div>
                                        </div>
                                    </div>


                                    <div class="trow">
                                        <div class="tcolumne">UEFA Champions League</div>
                                        <div class="tcolumn">2021/02/24</div>
                                        <div class="tcolumn">Atletico Madrid</div>
                                        <div class="tcolumnes">0-1</div>
                                        <div class="tcolumn">Chelsea</div>
                                        <div class="tcolumn">0-0</div>
                                        <div class="tcolumn">
                                            <div className="tcolumnees">
                                                W
                                            </div>  
                                        </div>
                                    </div>

                                    <div class="trow">
                                        <div class="tcolumne">English Premier League</div>
                                        <div class="tcolumn">2021/02/20</div>
                                        <div class="tcolumn">Southampton</div>
                                        <div class="tcolumnee">1-1</div>
                                        <div class="tcolumn">Chelsea</div>
                                        <div class="tcolumn">1-0</div>
                                        <div class="tcolumn">
                                            <div className="letter-D">
                                                D
                                            </div>
                                        </div>
                                    </div>

                                    <div class="trow">
                                        <div class="tcolumne">English Premier League</div>
                                        <div class="tcolumn">2021/02/16</div>
                                        <div class="tcolumn">Chelsea</div>
                                        <div class="tcolumnes">2-0</div>
                                        <div class="tcolumn">Newcastle United</div>
                                        <div class="tcolumn">2-0</div>
                                        <div class="tcolumn">
                                            <div className="tcolumnees">
                                                W
                                            </div>  
                                        </div>
                                    </div> 

                           </div>
                                <div className="foot">
                                        Last 5, Chelsea Win
                                    <span className="color-green">3</span>
                                        , Draw
                                    <span className="color-yellow">2</span>
                                        ,Lose
                                    <span className="color-red">0</span>
                                    , Score Win Prob : 60.00% 
                                </div>
                    
                 </div>
        
                </div>
                {/* start of mobile view */}
                <div className="mobileview">
                    <div className="container nopad">
                         <div className="match nopad">
                                <span className="match-update nopad xxsm">Latest Matches</span>&nbsp;
                                    <span className="match-team nopad xxsm">Chelsea</span>
                         </div>
                <div className="tab-bar justify-content no-border glassbg py-2">
                                	<div className="row">
                                  <div className="foot xxsm">
                                        Last 5, Chelsea Win
                                    <span className="color-green"> 3 </span>
                                        , Draw
                                    <span className="color-yellow"> 2 </span>
                                        , Lose
                                    <span className="color-red "> 0 </span>
                                    , Score Win Prob : 60.00% 
                                </div>
                                <div className="boxxx">
                                  <div className="row small pt-2">
                                 <div className="col-2 center lightgold-text padding-l20 xxsm"></div>
                                  <div className="col-8 center wrapper white-text xxsm">Atletico Madrid</div>
                                  <div className="col-1 padding-l5 lightgold-text strong xxsm"> 1 </div>
                                 </div>
                                  <div className="row">
                                      <div className="col-6 left padding-l20 lightgold-text xxsm">2021/02/24</div>
                                <div className="col-6 right  lightgold-text xxsm" style={{paddingRight:'18px'}}> D</div>
                                </div>
                                   <div className="row small pt-2">
                                 <div className="col-2 center lightgold-text padding-l20 xxsm"></div>
                                  <div className="col-8 center wrapper white-text xxsm">Chelsea</div>
                                  <div className="col-1 padding-l5 lightgold-text strong xxsm"> 1 </div>
                                 </div>
                                 
                            </div>
                            
                            
                        </div>
                        
                        <div className="boxxx">
                                  <div className="row small pt-2">
                                 <div className="col-2 center lightgold-text padding-l20 xxsm"></div>
                                  <div className="col-8 center wrapper white-text xxsm">Atletico Madrid</div>
                                  <div className="col-1 padding-l5 lightgold-text strong xxsm"> 1 </div>
                                 </div>
                                  <div className="row">
                                      <div className="col-6 left padding-l20 lightgold-text xxsm">2021/02/24</div>
                                <div className="col-6 right  lightgold-text xxsm" style={{paddingRight:'18px'}}> D</div>
                                </div>
                                   <div className="row small pt-2">
                                 <div className="col-2 center lightgold-text padding-l20 xxsm"></div>
                                  <div className="col-8 center wrapper white-text xxsm">Chelsea</div>
                                  <div className="col-1 padding-l5 lightgold-text strong xxsm"> 1 </div>
                                 </div>
                                 
                            </div>

                            <div className="boxxx">
                                  <div className="row small pt-2">
                                 <div className="col-2 center lightgold-text padding-l20 xxsm"></div>
                                  <div className="col-8 center wrapper white-text xxsm">Atletico Madrid</div>
                                  <div className="col-1 padding-l5 lightgold-text strong xxsm"> 1 </div>
                                 </div>
                                  <div className="row">
                                      <div className="col-6 left padding-l20 lightgold-text xxsm">2021/02/24</div>
                                <div className="col-6 right  lightgold-text xxsm" style={{paddingRight:'18px'}}> D</div>
                                </div>
                                   <div className="row small pt-2">
                                 <div className="col-2 center lightgold-text padding-l20 xxsm"></div>
                                  <div className="col-8 center wrapper white-text xxsm">Chelsea</div>
                                  <div className="col-1 padding-l5 lightgold-text strong xxsm"> 1 </div>
                                 </div>
                                 
                            </div>

                    </div>
                    
                    </div>
                    
                </div>

            </>
        )
    }
}
export default H2h
