import React, { Component } from 'react'

import SoccerLineUp from 'react-soccer-lineup'
import swal from 'sweetalert'




class LineUp extends Component {
   constructor(props) {
        super(props)

        this.state = {
            color: '588f58' ,
            pattern: 'lines',
            showHomeTeam: true,
            showAwayTeam: true,

            homeTeamColor: 'f08080',
            homeTeamNumberColor: 'ffffff',
            homeGoalkeeperColor: 'd6cb65',
            homeGoalkeeperNumberColor: '333333',
            homeTeamClickable: true,

            awayTeamColor: 'add8e6',
            awayTeamNumberColor: '333333',
            awayGoalkeeperColor: '4f6c75',
            awayGoalkeeperNumberColor: 'ffffff',
            awayTeamClickable: true
        }
    }
    render () {
            const { color, pattern, showHomeTeam, showAwayTeam } = this.state


        

        return (
            <>
            
            <div className="mobile"> 
                             <SoccerLineUp 
                    size="responsive"
                    color={ `#${ color }`}
                    pattern={ pattern }
                  homeTeam={ showHomeTeam ? this.buildHomeTeam() : undefined }
                  
                    awayTeam={ showAwayTeam ? this.buildAwayTeam() : undefined }
                
                  />
              
              </div>
                    
                       
                  <div className="mobileview"> 
                   <div className="rotate">
                             <SoccerLineUp 
                    size="small"
                    color={ `#${ color }`}
                    pattern={ pattern }
                  homeTeam={ showHomeTeam ? this.buildHomeTeam() : undefined }
                  
                    awayTeam={ showAwayTeam ? this.buildAwayTeam() : undefined }
                
                  /></div>
              
              </div>
               
            </>
        
        )

    }
    buildHomeTeam = () => {

        const {
            homeTeamColor,
            homeTeamNumberColor,
            homeGoalkeeperColor,
            homeGoalkeeperNumberColor,
            homeTeamClickable
        } = this.state

        
        return {
            
            squad: {
                
                gk: {
                    number: 1,
                    color: `#${ homeGoalkeeperColor }`,
                    numberColor: `#${ homeGoalkeeperNumberColor }`,
                    onClick: homeTeamClickable ? (() => swal(`Home team - Player ${ 1 }`)) : undefined
                },
                df: [ {
                    number: 2,
                         color: `#${ homeTeamColor }`,
                    numberColor: `#${ homeTeamNumberColor }`,
                    onClick: homeTeamClickable ? (() => swal(`Home team - Player ${ 2 }`)) : undefined
                }, {
                    number: 4,
                    onClick: homeTeamClickable ? (() => swal(`Home team - Player ${ 4 }`)) : undefined
                }, {
                    number: 5,
                    onClick: homeTeamClickable ? (() => swal(`Home team - Player ${ 5 }`)) : undefined
                }, {
                    number: 3,
                    onClick: homeTeamClickable ? (() => swal(`Home team - Player ${ 3 }`)) : undefined
                } ],
                cm: [ {
                    number: 6,
                    onClick: homeTeamClickable ? (() => swal(`Home team - Player ${ 6 }`)) : undefined
                }, {
                    number: 8,
                    onClick: homeTeamClickable ? (() => swal(`Home team - Player ${ 8 }`)) : undefined
                } ],
                cam: [ {
                    number: 11,
                    onClick: homeTeamClickable ? (() => swal(`Home team - Player ${ 11 }`)) : undefined
                }, {
                    number: 10,
                    onClick: homeTeamClickable ? (() => swal(`Home team - Player ${ 10 }`)) : undefined
                }, {
                    number: 7,
                    onClick: homeTeamClickable ? (() => swal(`Home team - Player ${ 7 }`)) : undefined
                } ],
                fw: [ {
                    number: 9,
                    onClick: homeTeamClickable ? (() => swal(`Home team - Player ${ 9 }`)) : undefined
                } ]
            },
            style: {
                color: `#${ homeTeamColor }`,
                numberColor: `#${ homeTeamNumberColor }`,
                
            }
        }
        
        
        
    }
   
      buildAwayTeam = () => {

        const {
            awayTeamColor,
            awayTeamNumberColor,
            awayGoalkeeperColor,
            awayGoalkeeperNumberColor,
            awayTeamClickable
        } = this.state

        return {
            squad: {
               
                df: [ {
                    number: 2,
                    onClick: awayTeamClickable ? (() => swal(`Away team - Player ${ 2 }`)) : undefined
                }, {
                    number: 4,
                    onClick: awayTeamClickable ? (() => swal(`Away team - Player ${ 4 }`)) : undefined
                }, {
                    number: 5,
                    onClick: awayTeamClickable ? (() => swal(`Away team - Player ${ 5 }`)) : undefined
                }, {
                    number: 3,
                    onClick: awayTeamClickable ? (() => swal(`Away team - Player ${ 3 }`)) : undefined
                } ],
                cam: [ {
                    number: 7,
                    onClick: awayTeamClickable ? (() => swal(`Away team - Player ${ 7 }`)) : undefined
                }, {
                    number: 8,
                    onClick: awayTeamClickable ? (() => swal(`Away team - Player ${ 8 }`)) : undefined
                }, {
                    number: 6,
                    onClick: awayTeamClickable ? (() => swal(`Away team - Player ${ 6 }`)) : undefined
                }, {
                    number: 10,
                    onClick: awayTeamClickable ? (() => swal(`Away team - Player ${ 10 }`)) : undefined
                } ],
                fw: [ {
                    number: 9,
                    onClick: awayTeamClickable ? (() => swal(`Away team - Player ${ 9 }`)) : undefined
                }, {
                    number: 11,
                    onClick: awayTeamClickable ? (() => swal(`Away team - Player ${ 11 }`)) : undefined
                } ],

                gk: {
                    number: 1,
                    color: `#${ awayGoalkeeperColor }`,
                    numberColor: `#${ awayGoalkeeperNumberColor }`,
                    onClick: awayTeamClickable ? (() => swal(`Away team - Player ${ 1 }`)) : undefined
                },


            },
            style: {
                color: `#${ awayTeamColor }`,
                numberColor: `#${ awayTeamNumberColor }`
            }
        
        }
        
    }
    
}

export default LineUp