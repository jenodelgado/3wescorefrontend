import React, {Component} from 'react'
import '../../assets/components/shortstat.css'

class ShortStat extends Component {
	constructor(props) {
		super(props)
	}

	render() {
		return (<div className="container short-stat-main-container white-text small">
            <div className="row center py-2">
                <div className="col-md-3 with_divider">
                    <div className="row">
                        <div className="col-sm-12 py-2 with_underline">1 x 2</div>
                        <div className="col-sm-12 py-2">
                            <div className="row">
                                <div className="col-sm-4">126.0</div>
                                <div className="col-sm-4">34.0</div>
                                <div className="col-sm-4">1.005</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-md-3 with_divider">
                    <div className="row">
                        <div className="col-sm-12 py-2 with_underline">Handicap</div>
                        <div className="col-sm-12 py-2">
                            <div className="row">
                                <div className="col-sm-3">-0.25</div>
                                <div className="col-sm-3">3.70</div>
                                <div className="col-sm-3">+0.25</div>
                                <div className="col-sm-3">1.28</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-md-3 with_divider">
                    <div className="row">
                        <div className="col-sm-12 py-2 with_underline">O / U</div>
                        <div className="col-sm-12 py-2">
                            <div className="row">
                                <div className="col-sm-4">2.5</div>
                                <div className="col-sm-4">4.50</div>
                                <div className="col-sm-4">1.21</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-md-3">
                    <div className="row">
                        <div className="col-sm-12 py-2 with_underline">Corner</div>
                        <div className="col-sm-12 py-2">
                            <div className="row">
                                <div className="col-sm-4">7.5</div>
                                <div className="col-sm-4">0.78</div>
                                <div className="col-sm-4">1.02</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>)
	}
}

export default ShortStat
