import React, { Component } from 'react'
import { Link } from 'react-router-dom'

class StartMatchTitle extends Component {

	render() {
		let data = (this.props.state && this.props.state.match) ? this.props.state.match : {}
		console.log('data', data)
		return (
			<>
			<div className="mobile">	
					<div className="container">
		<div className="row white-text" style={{ 'border-bottom': '1px solid #4b4b4b' }}>
			<div className="col-md-4">
				<div className="row py-4">
					<div className="col-sm-10 large right"> { data.Home } </div>
					<div className="col-sm-2"> <img style={{ 'max-width': '50px' }} src="https://www.auszac.com/wp/wp-content/uploads/2017/04/circle-placeholder.png"/> </div>
				</div>
			</div>
			<div className="col-md-4">
				<div className="row py-4">
					<div className="col-sm-4 center large strong"> { data.HomeScore } </div>
					<div className="col-sm-4 center mt-3"> - </div>
					<div className="col-sm-4 center large strong"> { data.AwayScore } </div>
				</div>
			</div>
			<div className="col-md-4">
				<div className="row py-4">
					<div className="col-sm-2"> <img style={{ 'max-width': '50px' }} src="https://www.auszac.com/wp/wp-content/uploads/2017/04/circle-placeholder.png"/> </div>
					<div className="col-sm-10 large"> { data.Away } </div>
				</div>
			</div>
		</div>
			</div>
			</div>
			{/* start of mobile view */}
				<div className=" mobileview">
					<div className="container">
					
					</div>

				</div>

			{/* end of mobile view */}
		</>

		);
	}
}

export default StartMatchTitle