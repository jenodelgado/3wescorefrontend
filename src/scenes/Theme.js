/**
* @author Joan Villariaza (joan.villariaza@gmail.com)
*/
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Redirect  } from 'react-router-dom'
import PropTypes from "prop-types";

import Navbar from './modules/fixUI/header'
import Footer from './modules/fixUI/Footer'

import '../App.css'
import '../assets/components/modal.css'
import '../assets/themeSelector.css'

import LeaguesHandler from '../services/handlers/Leagues'
import { LEAGUE_REQUEST, LEAGUE_REQUEST_SUCCESS, LEAGUE_REQUEST_ERROR } from '../services/actions/League'
import { sortByLeagueAndStatus } from '../services/transformers/League'

class Theme extends Component {
	constructor(props) {
		super(props)
		this.state = {
			activeSport: 'FOOTBALL',
			sportIndex: 0
		}

		this.updateSport = this.updateSport.bind(this)
	}

	componentWillMount() {
		if (
			this.props.history && 
			this.props.history.location && 
			this.props.history.location.state && 
			this.props.history.location.state.sport 
		) {
			const { sport, index } = this.props.history.location.state
			this.props.dispatch(LeaguesHandler.getAllMatches(sport))
			this.setState({ activeSport: sport, sportIndex: index })

		} else {
			this.props.dispatch(LeaguesHandler.getAllMatches('FOOTBALL'))
		}
	}

	componentWillReceiveProps(nextProps) {
		this.setState({ leagues: nextProps.leagues })
	}

	getAllMatches(sport, index) {
		const { dispatch, leagues } = this.props
		const { all, live, finished } = sortByLeagueAndStatus(leagues._unfiltered, sport)

		dispatch({ 
			type: LEAGUE_REQUEST_SUCCESS, 
			_unfiltered: leagues._unfiltered,
			all: all,
			live: live,
			finished: finished
		})

		this.props.history.replace({pathname: '/', state: { sport, index }})
	}

	updateSport(sport, index) {
		this.getAllMatches(sport, index)
		this.props.setSport(sport)
		this.setState({ activeSport: sport, sportIndex: index })
	}

	render() {
        const { activeSport, sportIndex } = this.state

        return ( <div>
            <div className={`full-width full-height ${activeSport}`}>
                <Navbar { ...this.props } selectSportTheme={this.updateSport} sportIndex={sportIndex} />
                { this.props.children }
            </div> 

          <div className="mobile"> <Footer /></div> 
		  <div className="mobileviews"><Footer/></div>
        </div>)
	}
}

const mapStateToProps = (state, ownProps) => ({
	leagues: state.League
})

export default connect( mapStateToProps )( Theme )