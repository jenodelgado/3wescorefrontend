import React, { Component } from 'react'

import FootballMatches from  '../modules/FootballMatches'
import H2H from './H2H'
import Oddds from './Oddds'
import Lineups from './Lineups'
import Standinggs from './Standinggs'
import ThemeSelector from '../Theme'
import Breadcrumb from '../modules/Breadcrumb'
import StartMatchTitle from '../modules/StartMatchTitle'
import MatchMenu from '../modules/MatchMenu'
import {
	  Route,
	  NavLink,
	  HashRouter
	} from "react-router-dom";

class FootballMatchPage extends Component {
	render() {
		return ( 
			<>
            <FootballMatches/>
            </>
		)
	}
}

export default FootballMatchPage