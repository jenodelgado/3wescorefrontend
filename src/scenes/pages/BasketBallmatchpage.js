import React, { Component } from 'react'
import ReactPlayer from "react-player";
import ThemeSelector from '../Theme'
import ChatBox from '../modules/widgets/chat/ChatBox'
import ShortStat from '../modules/ShortStat'
import Stats from '../modules/FootballStats'
import Standings from '../modules/Standings'
import MatchInfo from '../modules/MatchInfo'
import Breadcrumb from '../modules/Breadcrumb'
import StartMatchTitle from '../modules/StartMatchTitle';
import MatchMenu from '../modules/MatchMenu'

class BasketBallmatchpage extends Component {
	render() {
		return ( <ThemeSelector>
			<div style={{ background: '#4b4b4b' }}>
				<div className="container py-2">
					<Breadcrumb />
				</div>
			</div>
			<div className="no-border glassbg">
				<div className="container py-2">
					{/* Start of Match Title */}
					{/* TO DO: Make this a reusable component */}
					<StartMatchTitle/>
					{/* End of Match Title */}

					{/* Start of Match Menu */}
					{/* TO DO: Make this a reusable component */}
					<MatchMenu />
					{/* End of Match Menu */}


					{/* Start of Video and Chat modules */}
					<div className="row mt-1">
						<div className="col-md-8"><ReactPlayer width="100%" height="100%" className="videocss" url="https://www.youtube.com/watch?v=zqeUpgCpAdE" /></div>
						<div className="col-md-4 mt-1 padding-l5"><ChatBox /></div>
					</div>
					{/* End of Video and Chat modules */}

					{/*Game Stats and details*/}
					<div className="row py-1">
						<div className="col-md-8">
							<div className="row">
								<div className="col-md-12 mb-2"><ShortStat /></div>
								<div className="col-md-6"><Standings /></div>
								<div className="col-md-6"><Stats /></div>
							</div>
						</div>
						<div className="col-md-4"><MatchInfo /></div>
					</div>
					{/*End of game stats and details*/}

				</div>
			</div>
		</ThemeSelector> )
	}
}

export default BasketBallmatchpage