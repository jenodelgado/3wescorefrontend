import React, { Component } from 'react'
import Breadcrumb from '../modules/Breadcrumb'
import StartMatchTitle from '../modules/StartMatchTitle'
import ThemeSelector from '../Theme'
import MatchMenu from '../modules/MatchMenu'
import H2h from '../modules/H2h'
import ScheduledMatches from '../modules/ScheduledMatches'
import { HashRouter } from 'react-router-dom'
import ChatBox from '../modules/widgets/chat/ChatBox'

 class H2H extends Component {
    render() {
        return (
            <>
                
                
                    <div className="container">
                    <div className="row mt-1">
                        <ChatBox/>
                    </div>
                    </div>
              </> 
                
            
        )
    }
}
export default H2H
