import React, { Component } from 'react'
import ThemeSelector from '../Theme'
import '../../assets/TOS.css'
import Breadcrumb from '../modules/Breadcrumb'



class TermsOfService extends Component {
	render() {
		return ( <ThemeSelector>
            <>
					<div style={{ background: '#4b4b4b' }}>
				<div className="container py-2">
					<Breadcrumb />
				</div>
                </div>
            <div className="container">
			<div className="no-border glassbg">
                    <p className="headTOS">Privacy Policy</p>

                    <p className="headTOS">1 Privacy Policy of www.3wescore.com</p>
                <p className="contents">This privacy policy concerns the products of 3wescore (anything marked as ‘we, us, our’ and similar in this policy will refer to 3wescore) – our website www3wescore.com and our mobile applications. In order to provide our services, we have to collect some personal data. This privacy policy explains what data do we collect, how and why do we process and keep it, how you can contact us and find out about your privacy rights, as well as show our commitment to protection of your information.</p>
                <p className="headTOS">1.2 DATA CONTROLLER AND OWNER</p>
				<p className="contents">the owner of www.3wescore.com. You can contact us by e-mail on the address service@3wescore.com</p>
                <p className="headTOS">1.3 THIRD-PARTY LINKS</p>
                <p className="contents">Please do keep in mind that we aren’t in control of the data our partner websites collect and share. While we are committed to guarding your privacy and safety in every way possible, if you’re leaving 3wescore.com by clicking on a plug-in or a link you find on our site, personal data they collect is out of our hands. We strongly recommend you to read the privacy policies of those sites first.</p>
                <p className="headTOS">2 Types of Data collected – what do we collect?</p>
                <p className="headTOS">2.1 COLLECTED DATA</p>
                <p className="contents">Among the types of personal data that we collect, by itself or through third parties, are: Email, Cookie and Usage Data. If we need any other personal data collected at some point, it will either be described in other sections of this privacy policy or by dedicated explanation text contextually with the data collection, meaning you will be asked and fully informed about it. That means the personal data we collect may be freely provided by the user, or collected </p>
                <p className="headTOS">2.2 USER ACCOUNT</p>
                <p className="contents">In order to provide a better service for our users, we have — for a long time — had a feature of user account. Now, with your privacy more important than ever before, we are clarifying the way your personal data concerning user account is collected and stored.</p>
                <p className="contents">Personal data refers to information that can identify you, such as your name, e-mail address, or any data you provide while using 3wescore user account. Our login providers (Facebook or Google) provide us with some of your personal data — name, surname, e-mail — upon your login to our services. However, your personal information of that kind will never be sold or rented to anyone, for any reason, at any time. We will not store this information in any available database. This information will only be used to easier fulfill your requests for service, such as providing access to your in-app stats and features, and to enforce our Terms of Use.</p>
                <p className="headTOS">2.3 NOT-COLLECTED DATA</p>
                <p className="contents">At no point do we collect personal or sensitive personal data that could lead to a person being positively identified. Also, we never collect the data about race or ethnic origin, political opinions, religious or philosophical beliefs, trade union memberships, genetic or biometric data, health or mortality, sex life or sexual orientation, unless we are legally required by the court of law to do it.</p>
                <p className="headTOS">3 Mode and place of processing the Data – how do we collect?</p>
                <p className="headTOS">3.1 METHOD OF PROCESSING</p>
                <p className="contents">Our Data Controller processes the data of our users in proper manner and shall take appropriate security measures to prevent unauthorized access, disclosure, modification or unauthorized destruction of the data.</p>
                <p className="contents">The data processing itself is carried out using computers and/or IT enabled tools, following organizational procedures and modes strictly related to the purposes indicated. In addition to the Data Controller, in some cases, the data may be accessible to certain types of persons in charge, involved with the operation of the site (administration, sales, marketing, legal, system administration) or external parties (such as third party technical service providers, mail carriers, hosting providers, IT companies, communications agencies) appointed, if necessary, as data processors.</p>
                <p className="contents">What is important is that the updated list of these parties may be requested from the Data Controller at any time, so you will always know who is in charge.</p>
                <p className="headTOS">3.2 PLACE OF PROCESSING</p>
                <p className="contents">The data is processed at the Data Controller’s operating offices and in any other places where the parties involved with the processing are located. For further information, please contact the Data Controller.</p>
                <p className="headTOS">3.3 CONSERVATION TIME</p>
                <p className="contents">We keep the data for the time necessary to provide the service requested by the user, or stated by the purposes outlined in this document. You can always, at any time, and for any reason, request the Data Controller for the suspension or removal of your data.</p>
                <p className="headTOS"> The use of the collected Data – why do we collect?</p>
                <p className="contents">The data concerning the user is collected to allow us to provide our services, as well as for the following purposes: contacting the user and displaying content from external platforms.</p>
                <p className="headTOS">4.1 CONTACTING THE USER</p>
                <p className="contents">Mailing List or Newsletter (This Application)</p>
                <p className="contents"> Personal Data collected in this case is your e-mail address.</p>
                <p className="contents">By registering to the mailing list or to the newsletter, the user’s email address will be added to the contact list of those who may receive email messages containing information of commercial or promotional nature concerning our product.</p>
                <p className="headTOS">4.2 DISPLAYING CONTENT FROM EXTERNAL PLATFORMS</p>
                <p className="contents">Personal Data collected in this case are your Cookie and Usage Data.</p>
                <p className="headTOS">5 Further information about Personal Data</p>
                <p className="headTOS">5.1 PUSH NOTIFICATIONS</p>
                <p className="contents">We may send push notifications to you. Push notification may include alerts, sounds, icon badges and other information in relation to the use of our products. You can choose to allow or reject push notifications being sent to your device — if you do not allow us to send you push notifications, you will still be able to use our products freely, but you may not get the full benefit of the features. You can control your preferences to receive push notifications via your device settings.</p>
                <p className="headTOS">5.2 UNIQUE DEVICE IDENTIFICATION</p>
                <p className="contents">Sometimes, our products may store some unique identifiers of your device, only for analytics purposes or for storing your preferences.</p>
                <p className="headTOS">6 Additional information about Data collection and processing</p>
                <p className="headTOS">6.1 LEGAL ACTIONS</p>
                <p className="contents">As we already mentioned, your data may be used for legal purposes by the Data Controller, in Court or in the stages leading to possible legal action arising from improper use of our products or the related services.</p>
                <p className="headTOS">6.2 SYSTEM LOGS AND MAINTENANCE</p>
                <p className="contents">For operation and maintenance purposes, we may sometimes collect files that record your interaction with our products (so called System Logs) or use for this purpose other personal data (such as IP Address). That data won’t be stored permanently and won’t be available to sell or rent to anyone.</p>
                <p className="headTOS">6.3 YOUR RIGHTS</p> 
                <p className="contents">When it comes to having access to your personal data, you have the biggest rights. By submitting a user access request, we will provide you the following information free of charge:</p>  
                 <p className="contents"><span>&#8226; </span>What personal information pertaining to you is being processed</p>            
                    <p className="contents"><span>&#8226; </span>Why this information is being processed</p>          
                    <p className="contents"><span>&#8226; </span>Who has access to this personal information about you</p>  
                     <p className="contents"><span>&#8226; </span>How this personal information is being used in automated decisions</p>  
                     <p className="contents"><span>&#8226; </span>What processes are using this information</p>  
              <p className="contents">A user access request should be completed within 30 days and include a copy of the personal information, so we can be sure it’s you asking, and not some impersonator.</p> 
              <p className="contents">Keep in mind our products do not support “do not track” requests. To understand if any of the third party services we use honor the “do not track” requests, please read their privacy policies.</p>
            <p className="headTOS">6.4 MORE INFO</p>
                <p className="contents">More details concerning the collection or processing of personal data may be requested from the Data Controller at any time at its contact information. The data controller also has the right to make changes to this privacy policy at any time by giving notice to you on this page. It is strongly recommended to check this page often, referring to the date of the last modification listed at the bottom. If you object to any of the changes to the policy — which is your legitimate right — please request from us to erase the personal data so you can cease using our products. Unless stated otherwise, the then-current privacy policy applies to all personal data the Data Controller has about users. The Data Controller is responsible for this privacy policy.</p>
                <p className="headTOS">7 Legal information</p>
                <p className="contents">This privacy policy was last updated on March 8, 2021.</p>
               <p className="headTOS"> 8 CONTACT</p>
               <p className="contentsEnd">please email us at service@3wescore.com</p>


            </div>
			</div>
            </>
		</ThemeSelector> )
	}
}

export default TermsOfService