import React, { Component } from 'react'

import ThemeSelector from '../Theme'

import MatchMenu from '../modules/MatchMenu'
import H2H from './H2H'
import Oddds from './Oddds'
import Lineups from './Lineups'
import Standinggs from './Standinggs'
import FootballMatchPage from './FootballMatch'
import Breadcrumb from '../modules/Breadcrumb'
import StartMatchTitle from '../modules/StartMatchTitle'
import FootballMatches from '../modules/FootballMatches'
import SoccerLineUp from '../modules/SoccerLineUp'

import {
	  Route,
	  NavLink,
	  HashRouter
	} from "react-router-dom";
import FootballPage from './FootballPage';

class ForTesting extends Component {
	render() {
		return ( 
			<ThemeSelector>
				<HashRouter>
				<div style={{ background: '#4b4b4b' }}>
                    <div className="container py-2">
                        <Breadcrumb/>
                    </div>
                </div>
                <div className="no-border glassbg">
                    <div className="container py-2">
                        <StartMatchTitle/>
                        <MatchMenu />
						
					
				<Route  exact path="/" component={FootballMatches} />
				<Route path="/h2h" component={H2H} />
				<Route path="/oddds" component={Oddds} />
				<Route path="/lineups" component={Lineups}/>
				<Route path="/Standinggs" component={Standinggs} />
				</div>
				</div>
			</HashRouter>
				

			</ThemeSelector>
		)
	}
}

export default ForTesting