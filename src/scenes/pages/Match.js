import React, { Component } from 'react'
import { connect } from 'react-redux'
import Iframe from 'react-iframe'
import ThemeSelector from '../Theme'
import Breadcrumb from '../modules/Breadcrumb'
import StartMatchTitle from '../modules/StartMatchTitle'
import MatchMenu from '../modules/MatchMenu'
import ChatBox from '../modules/widgets/chat/ChatBox'
import LeaguesHandler from '../../services/handlers/Leagues'
import FootballMatches from './FootballMatch'
import H2H from './H2H'
import Oddds from './Oddds'
import Lineups from './Lineups'
import Standinggs from './Standinggs'
import { Route, NavLink, HashRouter } from "react-router-dom"
import Footer from '../modules/fixUI/Footer'
import Accordion from '../modules/accordion/Accordion'

import { GET_STREAM, GET_HORSE_STREAM } from '../../.env'

class MatchPage extends Component {
	constructor(props) {
		super(props)

		this.state = {
			data: {},
			sport: 'FOOTBALL'
		}
		
		this.setSport = this.setSport.bind(this)
	}

	componentWillMount() {
		console.log('this.props', this.props)
		if (this.props.location.state && this.props.location.state.match) {
			const { Channel, Type } = this.props.location.state.match
			this.props.dispatch(LeaguesHandler.getLiveStream( Channel, (!Type ? GET_HORSE_STREAM : GET_STREAM) ))
			this.setState({ data: this.props.location.state.match })
		}
	}

	setSport(sport) {
		this.setState({ sport })
	}

	render() {
		const { League, Name } = this.state.data
		const { leagues } = this.props

		let links = [
			{ name: League, link: '/' },
			{ name: Name },
		]
		return (<div>
			<HashRouter>
				<ThemeSelector {...this.props} setSport={this.setSport}>
					<div className="container mobile"> 
						<div style={{ background: '#4b4b4b' }}>
							<div className="container py-2">
								<Breadcrumb links={links} />
							</div>
						</div>
						<div className="no-border glassbg full-height">
		                    <div className="container py-2" >
		                        {/*<StartMatchTitle {...this.props.location} />*/}
		                        <MatchMenu />
								<div className="py-2 px-2" style={{ background: '#4b4b4b' }}>
									{/* Start of Video and Chat modules */}
									<div className="row height-65" >
										<div className="col-8 padding-r5">
											{<Iframe frameborder="0" width="100%" height="100%" scrolling="no" className="videocss" url={`${leagues.stream}&compact`} />}
										</div>
										<div className="col-4 mt-1 padding-l5"><ChatBox /></div>
									</div>
								</div>
							</div>
							
							<Route path="/h2h" component={H2H} />
							<Route path="/oddds" component={Oddds} />
							<Route path="/lineups" component={Lineups}/>
							<Route path="/Standinggs" component={Standinggs} />
						</div>
					</div>
					{/* start of mobile view */}
					<div className="mobileview">
						<div style={{ background: '#4b4b4b' }}>
							<div className="no-border glassbg full-height">
								<div className="row video-player-mobile">
									<div className="col-12">
										<div className="no-border px-1 pt-1 strong" style={{ height: '20px', position: 'absolute' }}><a className="gold-text" href="/"><i className="fas fa-chevron-circle-left"></i> BACK</a></div>
										{<Iframe frameborder="0" width="100%" height="100%" scrolling="no" className="videocss" url={`${leagues.stream}&compact`} />}
									</div>
								 	{/*<MatchMenu />*/}
								</div>
								<div className="row height-65">
									<div className="col-12">
										<ChatBox />
									</div>
								</div>
								<Route exact path="/chat" component={ChatBox}/>
								<Route path="/h2h" component={H2H} />
								<Route path="/oddds" component={Oddds} />
								<Route path="/lineups" component={Lineups}/>
								<Route path="/Standinggs" component={Standinggs} />
							</div>
						</div>
					</div>
				</ThemeSelector>
			</HashRouter>
		</ div> )
	}
}

const mapStateToProps = (state, ownProps) => ({
	leagues: state.League
})

export default connect( mapStateToProps )( MatchPage )