import React, { Component } from 'react'
import Breadcrumb from '../modules/Breadcrumb'
import StartMatchTitle from '../modules/StartMatchTitle'
import MatchMenu from '../modules/MatchMenu'
import ThemeSelector from '../Theme'
import Lineupss from '../modules/Lineupss'
import Fields from '../modules/Field'
import { HashRouter } from 'react-router-dom'
import SoccerLineUp from '../modules/SoccerLineUp'
import '../../assets/themeSelector.css'


 class Lineups extends Component {
    render() {
        return (
           
                <>
                    <div className="container">   
                        <div className="row">
                            <div className="col-12">
                                <div className="space">
                                    <SoccerLineUp /> 
                                </div>
                            </div>
                        </div>     
                    </div>
           </>
          
        )
    }
}
export default Lineups
