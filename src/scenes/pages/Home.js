/**
* @author Joan Villariaza (joan.villariaza@gmail.com)
*/
import React, { Component } from 'react'
import { HashRouter, Link } from 'react-router-dom'
import { TabList, Tab, TabPanel } from 'react-tabs'
import { connect } from 'react-redux'
import RenderIf from 'render-if'

import ThemeSelector from '../Theme'
import AccordionSidebarModule from '../modules/sidebar/AccordionSidebarModule'
import TabModule from '../modules/tabs/TabModule'
import Accordion from '../modules/accordion/Accordion'
import SinglePageTitle from '../modules/SinglePageTitle'
import "../../assets/themeSelector.css"
import Breadcrumb from '../modules/Breadcrumb'
import LeaguesHandler from '../../services/handlers/Leagues'
import PreLoader from '../modules/PreLoader'
import axios from 'axios';

import { LEAGUE_REQUEST, LEAGUE_REQUEST_SUCCESS, LEAGUE_REQUEST_ERROR } from '../../services/actions/League'
import { sortByLeagueAndStatus } from '../../services/transformers/League'
import Favs from '../modules/Favs'
class HomePage extends Component {
	constructor(props) {
		super(props)

		this.state = {
			leagues: this.props.leagues,
			tabIndex: 0,
			language: '',
			activeSport: 'FOOTBALL',
			datafavs: ''
		}
        this.setLanguage = this.setLanguage.bind(this)
        this.setSport = this.setSport.bind(this)
	
	}

	componentWillReceiveProps(nextProps) {
		this.setState({ leagues: nextProps.leagues })
	}

	setTabIndex(index) {
		this.setState({ tabIndex: index })
	}

	setLanguage(lang){
        this.setState({language: lang})
    }

    setSport(sport) {
    	this.setState({ activeSport: sport })
    }

	// componentDidMount() {
	// 	const apiUrl = 'http://127.0.0.1:8000/api/favorite';
	// 	fetch(apiUrl)
	// 	.then((response) => response.json())
	// 	.then((data) => console.log('daaaaaaaaata', data));
	// }

	

	render() {

		let tabs = [
			{ index: 0, label: 'All', icon: '' },
			{ index: 1, label: 'Live', icon: 'far fa-clock red-text mr-1 margin-r5' },
			{ index: 2, label: 'Finished', icon: '' },
			{ index: 3, label: 'Favorites', icon: '' }
		]
		
		
		const {language, leagues, tabIndex, activeSport } = this.state
	
		return (
			
			<ThemeSelector {...this.props} setLanguage={this.setLanguage} activeSport={activeSport} setSport={this.setSport}>
			<>	
			{/* <AccordionSection/> */}
			<div className="container no-margin mobile">
				{ RenderIf(activeSport == 'HORSERACING')(<AccordionSidebarModule leagues={leagues} language={language} sport={activeSport} />) }
				{ RenderIf(activeSport !== 'HORSERACING')(
					<div className="row"> 
						<div className="col-md-3 padding-r5">
							<AccordionSidebarModule leagues={leagues} language={language} />
						</div>

						<div className="col-md-9 mt-2 padding-l5">
							<TabModule selectedIndex={ tabIndex } onSelect={index => this.setTabIndex(index)}>
								<TabList> { tabs.map( tab => ( <Tab><i className={tab.icon}/> { tab.label }</Tab> )) }</TabList>
								<TabPanel> 
									{RenderIf(leagues.isFetching)(this.showLoader())}
									{RenderIf(!leagues.isFetching && !leagues.all.length)(<p className="px-3">Nothing to show.</p>)}
									<Accordion allowMultipleOpen>
										{RenderIf(!leagues.isFetching)(this.generateLeagues(leagues.all))}
									</Accordion>
								</TabPanel>
								<TabPanel> 
									{RenderIf(leagues.isFetching)(this.showLoader())}
									{RenderIf(!leagues.isFetching && !leagues.live.length)(<p className="px-3">Nothing to show.</p>)}
									<Accordion allowMultipleOpen>
										{RenderIf(!leagues.isFetching)(this.generateLeagues(leagues.live))}
									</Accordion>
								</TabPanel>
								<TabPanel>
								</TabPanel>
								<TabPanel>
									<Favs/>
								</TabPanel>
								{/*  */}
							</TabModule>
						</div>
					</div>
				)}
			</div>
			{/* this starts the mobile view */}
			<div className="container no-margin mobileview">
				{ RenderIf(activeSport == 'HORSERACING')(<AccordionSidebarModule leagues={leagues} language={language} sport={activeSport} />) }
				{ RenderIf(activeSport !== 'HORSERACING')(<div className="row"> 
					<div className="col-md-12 mt-2 nopad">
						<TabModule selectedIndex={ tabIndex } onSelect={index => this.setTabIndex(index)}>
							<TabList> { tabs.map( tab => ( <Tab><i className={tab.icon}/> { tab.label }</Tab> )) }</TabList>
							<TabPanel> 
								{RenderIf(leagues.isFetching)(this.showLoader())}
								{RenderIf(!leagues.isFetching && !leagues.all.length)(<p className="px-3">Nothing to show.</p>)}
								<Accordion allowMultipleOpen>
									{RenderIf(!leagues.isFetching)(this.generateLeagues(leagues.all))}
								</Accordion>
							</TabPanel>
							<TabPanel> 
								{RenderIf(leagues.isFetching)(this.showLoader())}
								{RenderIf(!leagues.isFetching && !leagues.live.length)(<p className="px-3">Nothing to show.</p>)}
								<Accordion allowMultipleOpen>
									{RenderIf(!leagues.isFetching)(this.generateLeagues(leagues.live))}
								</Accordion>
							</TabPanel>
							<TabPanel> 
								{RenderIf(leagues.isFetching)(this.showLoader())}
								{RenderIf(!leagues.isFetching && !leagues.finished.length)(<p className="px-3">Nothing to show.</p>)}
								<Accordion allowMultipleOpen>
									{RenderIf(!leagues.isFetching)(this.generateLeagues(leagues.finished))}
								</Accordion>
							</TabPanel>
						</TabModule>
					</div>
				</div>) }
			</div>
			{/* end of mobile view */}
		</>	
		</ThemeSelector>
		
		)
	}

	/**
	* TO DO: This should be outside and common. Find a better loader.
	*/
	showLoader() {
		return (
			<>
			<div className="container"> 
			<div className="py-4 center"><br/><br/><br/><br/><br/><br/><br/><br/><br/> <PreLoader/></div>
			</div>
			</>	
			)
	}
	
	generateLeagues(leagues) {
		const { language } = this.state
		
		return leagues.map(league => {
			let name = league[`name${language}`].split(/-(.+)/)
			return (<div 
				label={(name.length > 1) ? name[1] : name[0]}
				parentLabel={(name.length > 1) ? name[0] : ''}
				caretClass="gold-text"
				containerTitleClass="mb-1 mt-1 px-2" 
				contentClass="match-main-container"
				
				>

				<SinglePageTitle matches={ league.matches } getAllMatches={ this.getAllMatches } language={language} />
			</div>)
		})
	}
}

const mapStateToProps = (state, ownProps) => ({
	leagues: state.League
})

export default connect( mapStateToProps )( HomePage )