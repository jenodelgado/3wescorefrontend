import React, {Component} from 'react';
import './App.css';
import Navbar from './components/Navbar';
import SidebarModule from './scenes/modules/SidebarModule';
import TabModule from './scenes/modules/TabModule';
import {BrowserRouter as Router} from 'react-router-dom';
import './assets/components/modal.css';
import ChatBox from './scenes/modules/chat/ChatBox';

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  render() {
    return (
      <Router>
        <div style={{ minHeight: '100vh', background: 'url("/backgrounds/football.png")' }}>
          <Navbar />
          <div className="container">
              <div className="row">
                <div className="col-md-3 mx-1">
                  <div className="row mt-2 ml-2">
                    
                    <SidebarModule title="Popular" />
                    <SidebarModule title="Leagues" with_margin={true} />
                    <SidebarModule title="Countries" with_margin={true} />

                  </div>
                </div>

                <div className="col-md-8 border-gray mt-2">
                  <TabModule />
                  <ChatBox />
                </div>
              </div>
            </div>
        </div>
      </Router>
    ) 
  }
}

export default App;
