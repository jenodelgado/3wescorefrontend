import orderBy from 'lodash/orderBy'
import findIndex from 'lodash/findIndex'

export function sortByLeagueAndStatus(data=[], sport='FOOTBALL') {
	let parentLeagues = []
	let live = []
	let finished = []
	let ctr = 0
	
	if (sport != 'HORSERACING') {
		data.forEach(d => {
			if (d.Type == sport) {

				// ALL MATCHES
				let parent = parentLeagues.map( pl => pl.name == d.League ).filter(d => d)
				if (!parent.length) {
					parentLeagues.push({ name: d.League, nameCH: d.LeagueCH, nameTH: d.LeagueTH, matches: [ d ] })
				} else {
					let index = findIndex( parentLeagues, pl => pl.name == d.League )
					let _parent = parentLeagues[index]
						_parent.matches.push(d)

					parentLeagues[index] = _parent
				}

				// LIVE MATCHES
				if (d.IsLive == 1 && d.NowPlaying) {
					let _live = live.map( l => l.name == d.League ).filter(d => d)
					if (!_live.length) {
						live.push({ name: d.League, nameCH: d.LeagueCH, nameTH: d.LeagueTH, matches: [ d ] })
					} else {
						let index = findIndex( live, l => l.name == d.League )
						let __live = live[index]
							__live.matches.push(d)

						live[index] = __live
					}
				}

				// FINISHED MATCHES
				if (d.State == 'off') {
					let now = new Date().getTime()
					let timeStop = new Date(d.TimeStop).getTime()
					if (timeStop < now) {
						let _finished = finished.map( f => f.name == d.League ).filter(d => d)
						if (!_finished.length) {
							finished.push({ name: d.League, nameCH: d.LeagueCH, nameTH: d.LeagueTH, matches: [ d ] })
						} else {
							let index = findIndex( finished, f => f.name == d.League )
							let __finished = finished[index]
								__finished.matches.push(d)

							finished[index] = __finished
						}
					}
				}
			}

			ctr++
		})
	} else {
		// 
		ctr = data.length
	}

	if (ctr == data.length)
		return { all: parentLeagues, live: live, finished: finished }
}