import { SignUpService } from '../AuthService';
import { loginUser } from '../AuthService';

export const signUp = (credentials) => {
    console.log(credentials);
    return (dispatch) => {
        if(credentials.password.length < 6){
            return dispatch({type: 'SHORT_PASSWORD'})
        }
    
        SignUpService(credentials).then((res)=> {
            let text = JSON.parse(res.text)
            if(text && text.token!==null && text.success){
                localStorage.setItem("user", `Bearer ${text.token}`);
                dispatch({type:'SIGNUP_SUCCESS', payload: 'Registered Successfully!'})
            }else{
                dispatch({type:'SIGNUP_ERROR', payload: text.message})
            }
        },
        error=>{
            dispatch({type:'CODE_ERROR', error});
        }
        )
    }
}

export const LoginUser = (credentials, history) => {
    console.log(credentials);
    return (dispatch) => {
        loginUser(credentials, history).then((res) => {
                let text = JSON.parse(res.text)
                if(text && text.token!==null && text.success){
                    localStorage.setItem("user", `Bearer ${text.token}`);
                    dispatch({type:'LOGIN_SUCCESS'})
                    // history.push("/dashboard");
                }else{
                    dispatch({type:'SIGNUP_ERROR', payload: text.message})
                }
            },
            error=>{
                dispatch({type:'CODE_ERROR', error});
            })
    }
}