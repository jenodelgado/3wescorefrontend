const initState = {
    authResponse:null
}

const AuthReducer = (state=initState, action) =>{
    switch(action.type){
        case 'SHORT_PASSWORD':
            console.log(action);
            return{
                ...state,
                authResponse: 'Password is too short.'
            }

        case 'SIGNUP_SUCCESS':
            console.log(action);
            return{
                ...state,
                authResponse: action.payload,
            } 
        
        case 'SIGNUP_ERROR':
            console.log(action);
            return{
                ...state,
                authResponse: action.payload,
            } 

        case 'LOGIN_SUCCESS':
            console.log(action);
            return{
                ...state,
                authResponse: 'Redirect to dashboard!',
            } 

        case 'LOGIN_ERROR':
            console.log(action);
            return{
                ...state,
                authResponse: action.payload,
            } 

        // case 'CODE_ERROR':
        //     console.log(action);
        //     return{
        //         ...state,
        //         authResponse: action.payload,
        //     }

        

        default:
            return state;
    }
}


export default AuthReducer;