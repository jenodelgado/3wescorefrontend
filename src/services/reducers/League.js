import { LEAGUE_REQUEST, LEAGUE_REQUEST_SUCCESS, LEAGUE_REQUEST_ERROR, LEAGUE_STREAM_SUCCESS } from '../actions/League'
import { getAllByStatus } from '../transformers/League'

const League = function(
	state = { 
		isFetching: false,
		_unfiltered: [],
		all: [],
		live: [],
		finished: [],
		stream: ''
	}, 
	action
) {
	switch ( action.type ) {

		case LEAGUE_REQUEST: {
			return {
				...state,
				isFetching: true
			}
		}

		case LEAGUE_REQUEST_SUCCESS: {
			return {
				...state,
				isFetching: false,
				_unfiltered: (action._unfiltered) ? action._unfiltered : state._unfiltered,
				all: (action.all) ? action.all : state.all,
				live: (action.live) ? action.live : state.live,
				finished: (action.finished) ? action.finished : state.finished
			}
		}

		case LEAGUE_REQUEST_ERROR: {
			return {
				...state,
				isFetching: false
			}
		}

		case LEAGUE_STREAM_SUCCESS: {
			return {
				...state,
				isFetching: false,
				stream: (action.link) ? action.link : state.stream
			}
		}

		default: {
			return state
		}
	}
}

export default League