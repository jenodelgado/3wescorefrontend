const User = function(
	state = { data: [] },
	action
) {
	switch (action.type) {
		case 'USER_AUTHENTICATION_REQUEST': {
			return { data: [] }
		}

		default: {
			return state
		}
	}
}

export default User