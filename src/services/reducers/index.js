import { combineReducers } from "redux";
import User from './User'
import League from './League'
import AuthReducer from './AuthReducer';

export default combineReducers({
	User,
	League,
	auth:AuthReducer
});
