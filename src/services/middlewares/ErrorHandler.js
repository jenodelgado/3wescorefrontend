import Toast from 'toastr'

export const AUTH_CHECK = 'AUTH_CHECK'
export const AUTH_SUCCESS = 'AUTH_SUCCESS'
export const AUTH_FAILURE = 'AUTH_FAILURE'

export const errHandler = error => {
  let errTitle = 'ERROR: ';

  if (error && error.status) {
    switch (parseInt(error.status)) {
      case 401:
        let title = 'Expired Session';
        let msg = 'We failed to capture your session. You will be logged out automatically. Please log back in.';
        let timeout = 0;

        let responseBody = (error.response && error.response.body) ? error.response.body : null;
        if (responseBody) {
          title = 'Not Allowed.'
          timeout = 2000;

          if (responseBody && responseBody.status && responseBody.status.toUpperCase() === 'REJECTED') {
            msg = 'Inactive user, please contact admin.';
          } else if (responseBody && responseBody.status && responseBody.status.toUpperCase() === 'DISABLED') {
            msg = 'Suspended account, please contact admin.';
          }
        }

        Toast.error(title, msg);

        setTimeout(() => {
          // signOut();
        }, timeout);
        break;

      case 500:
        Toast.error('Internal Server Error', error);
        break;

      case 404:
        if (error.response.body && error.response.body.message) {
          Toast.error(error.response.body.message || `${errTitle}`)
        }
        break;

      default:
        errTitle = `${errTitle} ${error.status}`;
        console.error(`${errTitle}, ${error.message}`)

        if (error.response.body && error.response.body.message) {
          Toast.error(error.response.body.message || `${errTitle}`)

          if (Object.keys(error.response.body.errors || {}).length > 0) {
            Object.keys(error.response.body.errors).forEach(key => {
              let vError = error.response.body.errors[key];
              //console.log(key, vError);
              if (key === 'fw_users_tenant_email_unique') {
                let parts = vError.split('-');
                parts.shift();
                vError = parts.join('-');
              }
              Toast.error(vError);
            });
          }
        }
        break;
    }
  }

  return error;
}
