import _ from 'lodash/core';

export async function authHeader(headers) {
    let accessToken = false

    let defaults = {
        // 'Authorization': `Bearer ${Auth.getLocalToken()}`,
        // 'X-Requested-With': 'XMLHttpRequest',
        'Accept': 'application/json',
        'Authorization': accessToken,
    }

    let returnHeaders = headers ? headers : defaults;
    if (headers) returnHeaders['Authorization'] = accessToken;

    let requestHeaders = returnHeaders || {};
    return requestHeaders;
}
