import API, { customAPI } from '../middlewares/API'

export function getVisitorIPAddress() {
	return customAPI.get('https://www.cloudflare.com/cdn-cgi/trace')
		.then( d => {
			let ipRegex = /[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}/
			let ip = d.text.match(ipRegex)[0];
			return ip
		} )
}