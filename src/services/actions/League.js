export const LEAGUE_REQUEST = 'LEAGUE_REQUEST'
export const LEAGUE_REQUEST_SUCCESS = 'LEAGUE_REQUEST_SUCCESS'
export const LEAGUE_REQUEST_ERROR = 'LEAGUE_REQUEST_ERROR'
export const LEAGUE_STREAM_SUCCESS = 'LEAGUE_STREAM_SUCCESS'