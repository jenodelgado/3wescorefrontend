import API from './middlewares/API'

export const SignUpService = (credentials) => {
    return API.post('/user/register', credentials, false, true)
}

export const loginUser = (credentials) => {
    return API.post('/user/login', credentials, false, true)
}