import API from '../middlewares/API'
import { LEAGUE_REQUEST, LEAGUE_REQUEST_SUCCESS, LEAGUE_REQUEST_ERROR, LEAGUE_STREAM_SUCCESS } from '../actions/League'
import { sortByLeagueAndStatus } from '../transformers/League'
import { getVisitorIPAddress } from '../helpers/IP'

const Leagues = {
	getAllMatches: sport => (dispatch, getState) => {
		dispatch({ type: LEAGUE_REQUEST })
		
		API.get('/matches')
			.then(data => {
				let jsonData = JSON.parse(data.text)

				const { all, live, finished } = sortByLeagueAndStatus(jsonData.Match, sport)

				dispatch({ 
					type: LEAGUE_REQUEST_SUCCESS, 
					_unfiltered: jsonData.Match,
					all: all,
					live: live,
					finished: finished
				})
			})
	},
	getLiveStream: (channel, action) => (dispatch, getState) => {
		dispatch({ type: LEAGUE_REQUEST })
		
		getVisitorIPAddress().then(ip => {
			API.get(`/stream/${channel}/${ip}/${action}`)
				.then(data => {
					let jsonData = JSON.parse(data.text)

					dispatch({ 
						type: LEAGUE_STREAM_SUCCESS, 
						link: jsonData.H5LINKROW ? jsonData.H5LINKROW : jsonData.FLVPLAYERLINK
					})
				})
		})
	}
}

export default Leagues